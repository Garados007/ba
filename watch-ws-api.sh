#!/bin/bash

sudo docker run \
    -v "$(pwd)/paper/notes/websocket-api.yml:/data/api.yml" \
    -v "$(pwd)/bin:/data/bin" \
    -it \
    asyncapi/generator \
    -o /data/bin \
    /data/api.yml \
    @asyncapi/html-template \
    --force-write \
    --watch-template
