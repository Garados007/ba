namespace AsyncApiRenderer.Data;

public class ChannelInfo
{
    public string Description { get; set; } = "";

    public JsonSchemaNode Message { get; set; }
        = new JsonSchemaNode();

    public IEnumerable<string> GetMessageIds()
    {
        if (Message.OneOf is null)
            yield break;
        foreach (var entry in Message.OneOf)
        {
            const string prefix = "#/components/messages/";
            if (entry.Ref is null || !entry.Ref.StartsWith(prefix))
                continue;
            yield return entry.Ref.Substring(prefix.Length);
        }
    }
}
