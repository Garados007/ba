namespace AsyncApiRenderer.Data;

public class Channel
{
    public ChannelInfo Publish { get; set; }
        = new ChannelInfo();

    public ChannelInfo Subscribe { get; set; }
        = new ChannelInfo();
}
