namespace AsyncApiRenderer.Data;

public class DataFile
{
    public Info Info { get; set; }
        = new Info();

    public Dictionary<string, Channel> Channels { get; set; }
        = new Dictionary<string, Channel>();

    public Components Components { get; set; }
        = new Components();

    public void Deref()
        => Components.Deref();

    public void Collapse()
        => Components.Collapse();
}
