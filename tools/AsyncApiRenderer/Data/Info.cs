namespace AsyncApiRenderer.Data;

public class Info
{
    public string Title { get; set; } = "";

    public string Version { get; set; } = "";

    public string Description { get; set; } = "";
}
