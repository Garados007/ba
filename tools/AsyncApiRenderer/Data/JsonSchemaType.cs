namespace AsyncApiRenderer.Data;

public enum JsonSchemaType
{
    Null,
    Boolean,
    Object,
    Array,
    Integer,
    Number,
    String,
}
