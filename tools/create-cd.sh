#!/bin/bash

# create the file structure for the cd release

[ -d "cd" ] && rm -rf "cd"
mkdir "cd"
if [ "${CI_JOB_TOKEN}" = "" ]; then
    git clone https://gitlab.gwdg.de/max.brauer/ba.git cd/source
else
    git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.gwdg.de/max.brauer/ba.git cd/source
fi
rm -rf cd/source/.git

find cd/source |\
    xargs -I {} \
        touch --date="1970-01-01T00:00:00Z" -acm {}

# copy schemas
mkdir cd/schemas
cp cd/source/paper/doc/src/99-Anhang/994-Rollenschema/schema.json \
    cd/schemas/Rollenschema.json
cp cd/source/paper/doc/src/99-Anhang/993-config.json-Schema/schema.json \
    cd/schemas/config.json-schema.json
cp cd/source/paper/notes/websocket-api.yml cd/schemas/websocket-api.yml

mkdir cd/schema-files
cp -r cd/source/server/Dacryptero/schemas/* cd/schema-files

