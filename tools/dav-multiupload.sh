#!/bin/bash

# Uploads multiple files to the same remote target

webdav="$1"
credential="$2"
path="$3"

function simplify () {
    local name="$1"
    local edit="$(echo "$name" | sed "s@/\\./@/@" | sed -r "s@/([^\\.][^/]*|\\.[^\\./]+)/\\.\\./@/@")"
    if [[ "$name" = "$edit" ]]; then
        echo "$edit"
    else
        simplify "$edit"
    fi
}

shift 3
while (( "$#" )); do
    target="$(simplify "$path/$1")"
    echo "Upload $1 to $target"
    "$(dirname $0)/dav-upload.sh" "$webdav" "$credential" "$1" "$target"
    shift 1
done
