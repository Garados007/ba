using System;
using System.Collections.Generic;

namespace Dacryptero.Session
{
    public class ClientSession : DB.DBSession
    {
        public ReadOnlyMemory<byte> Id { get; }

        public string IdString => Convert.ToBase64String(Id.Span);

        public HashSet<string> Usernames { get; }
            = new HashSet<string>();

        public string Username
        {
            get
            {
                if (Usernames.Count == 0)
                    return "unauthorized user";
                return string.Join(", ", Usernames);
            }
        }

        public ClientSession()
        {
            var rng = new Random();
            Memory<byte> id = new byte[32];
            rng.NextBytes(id.Span);
            Id = id;
        }
    }
}