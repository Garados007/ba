using System;
using System.Threading.Tasks;
using Dacryptero.DB;
using Dacryptero.Web;
using Dacryptero.Web.Events.Receive;
using Fido2NetLib;

namespace Dacryptero.Session.Fido;

/// <summary>
/// Register a user to a new database that is in the state of beeing created.
/// </summary>
public class RegisterUserNew : RegisterUserBase
{
    public DB.Creation.NewDBInfo DB { get; }

    public RegisterUserNew(WebSocketConnection connection, Fido2Configuration configuration,
        AccountRegister account, DB.Creation.NewDBInfo db
    ) 
        : base(connection, configuration, account)
    {
        DB = db;
    }

    protected override ReadOnlyMemory<byte> GetDatabaseKey()
    {
        return DB.MasterKey;
    }

    protected override async Task AddAccount(DBAccess access)
    {
        DB.Accesses.Add(access);
        await Connection.Send(new Web.Events.Send.DatabaseAddRequestInfo(DB))
            .ConfigureAwait(false);
    }
}