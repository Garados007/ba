using Dacryptero.DB;
using Dacryptero.Web;
using Dacryptero.Web.Events.Receive;
using Fido2NetLib;
using System;
using System.Threading.Tasks;

namespace Dacryptero.Session.Fido;

public class RegisterUserEx : RegisterUserBase
{
    public DB.DBSingleController DB { get; }

    public ReadOnlyMemory<byte> DatabaseKey { get; }

    public RegisterUserEx(WebSocketConnection connection, Fido2Configuration configuration,
        AccountRegister account, DB.DBSingleController db, ReadOnlyMemory<byte> databaseKey
    )
        : base(connection, configuration, account)
    {
        DB = db;
        DatabaseKey = databaseKey;
    }

    protected override ReadOnlyMemory<byte> GetDatabaseKey()
    {
        return DatabaseKey;
    }

    protected override async Task AddAccount(DBAccess access)
    {
        DB.Config.Access.Add(access);
        await DB.Config.SaveAsync()
            .ConfigureAwait(false);
        await Connection.Send(new Web.Events.Send.InfoSend(Connection));
    }
}