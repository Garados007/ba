namespace Dacryptero.Search
{
    public enum SearchResultType
    {
        Interview,
        Person,
        File,
    }
}