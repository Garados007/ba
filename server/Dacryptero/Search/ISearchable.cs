using System.Collections.Generic;
using LiteDB;

namespace Dacryptero.Search
{
    public interface ISearchable
    {
        ObjectId Id { get; }

        IEnumerable<SearchPart> GetSearchParts();

        SearchResultType ResultType { get; }

        string GetName();

        string? GetRole();

        List<(string name, string value)> GetInfo();
    }
}