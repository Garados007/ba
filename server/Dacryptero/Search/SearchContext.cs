namespace Dacryptero.Search
{
    public class SearchContext
    {
        public SearchResultType SourceType { get; }

        public SearchPart Part { get; }

        public SearchContext(SearchResultType sourceType, SearchPart part)
        {
            SourceType = sourceType;
            Part = part;
        }
    }
}