using System.Threading;
using System.Threading.Tasks;
using Dacryptero.Data;
using Dacryptero.DB;

namespace Dacryptero.Search.AST
{
    public class RoleOption : OptionBase
    {
        public string Role { get; }

        protected override string Key => "role";

        public RoleOption(string role)
        {
            Role = role;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Role}";
        }

        public override SearchProgress Check(SearchContext context)
        {
#if DEBUG
            throw new System.InvalidOperationException("This should have been eliminated previously");
#else
            return new SearchProgress(1, new Collections.TreeCollection<ResultFound>());
#endif
        }

        public override Task<SearchIndex> GetSearchIndexAsync(DBSingleController db,
            IndexSource source, CancellationToken cancellationToken
        )
        {
            return Task.FromResult(SearchIndex.All);
        }
    }
}