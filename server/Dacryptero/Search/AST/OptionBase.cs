using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Search.AST
{
    public abstract class OptionBase
    {
        protected abstract string Key { get; }

        public override string ToString()
        {
            return $"{Key}:";
        }

        public abstract SearchProgress Check(SearchContext context);
        
        public abstract Task<SearchIndex> GetSearchIndexAsync(DB.DBSingleController db,
            Data.IndexSource source, CancellationToken cancellationToken
        );
    }
}