using System;

namespace Dacryptero.Search.AST
{
    public class AttributeOption : FieldOptionBase
    {
        protected override string Key => "attr";

        public AttributeOption(string name, FieldComparer comparer, string? value)
            : base(name, comparer, value)
        {
        }
    }
}