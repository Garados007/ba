namespace Dacryptero.Search.AST
{
    public class FieldOption : FieldOptionBase
    {
        protected override string Key => "field";

        public FieldOption(string name, FieldComparer comparer, string? value)
            : base(name, comparer, value)
        {
        }
    }
}