using System.Threading;
using System.Threading.Tasks;
using Dacryptero.DB;

namespace Dacryptero.Search.AST
{
    public class Succeed : AstBase
    {
        public static Succeed Default { get; } = new Succeed();

        public override SearchProgress Check(SearchContext context)
        {
            return new SearchProgress(1, new Collections.TreeCollection<ResultFound>());
        }

        public override AstBase Clone()
        {
            return this;
        }

        public override Task<SearchIndex> GetSearchIndexAsync(DBSingleController db,
            Data.IndexSource source, CancellationToken cancellationToken
        )
        {
            return Task.FromResult(SearchIndex.All);
        }
    }
}