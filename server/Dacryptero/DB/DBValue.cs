using System;
using System.Collections.Generic;

namespace Dacryptero.DB
{
    public readonly struct DBValue<T>
    {
        public DBValue(DBSingleController source, T value)
        {
            Value = value;
            Source = source;
        }

        public T Value { get; }

        public DBSingleController Source { get; }

        public override bool Equals(object? obj)
        {
            return obj is DBValue<T> value &&
                   EqualityComparer<T>.Default.Equals(Value, value.Value) &&
                   EqualityComparer<DBSingleController>.Default.Equals(Source, value.Source);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, Source);
        }

        public static bool operator ==(DBValue<T> left, DBValue<T> right)
            => left.Equals(right);

        public static bool operator !=(DBValue<T> left, DBValue<T> right)
            => !left.Equals(right);
    }
}