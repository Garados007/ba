using LiteDB;

namespace Dacryptero.DB.Migration
{
    public interface IMigrator
    {
        void Migrate(LiteDatabase database, DBConfig config, bool confidential);
    }
}