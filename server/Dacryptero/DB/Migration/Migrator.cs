using LiteDB;

namespace Dacryptero.DB.Migration
{
    public class Migrator
    {
        public void Migrate(LiteDatabase database, DBConfig config, bool confidential)
        {
            switch (database.UserVersion)
            {
                case 0:
                    new Version0Migration().Migrate(database, config, confidential);
                    break;
            }
        }
    }
}