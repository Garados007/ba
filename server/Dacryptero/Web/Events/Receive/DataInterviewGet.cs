using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class DataInterviewGet : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var id = Id.ToObjectId();
            DB.DBValue<Data.Interview>? interview = null;
            if (id != null)
            {
                interview = await args.Data.GetInterviewAsync(id, CancellationToken.None)
                    .ConfigureAwait(false);
            }
            await args.Connection.Send(new Send.DataInterviewSend(Id, interview))
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
        }
    }
}