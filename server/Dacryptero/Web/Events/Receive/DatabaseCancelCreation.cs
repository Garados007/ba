using System.Text.Json;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive;

public class DatabaseCancelCreation : ReceiveBase
{
    public string Key { get; private set; } = "";

    public override Task Execute(ExecuteArgs args)
    {
        args.Connection.NewDBInfo.TryRemove(Key, out _);
        return Task.CompletedTask;
    }

    public override void ReadJsonContent(JsonElement json)
    {
        Key = json.GetProperty("key").GetString() ?? "";
    }
}