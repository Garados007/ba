using System.Text.Json;
using System.Threading.Tasks;
using Dacryptero.Notifications;

namespace Dacryptero.Web.Events.Receive
{
    public class InfoRequest : ReceiveBase
    {
        public override async Task Execute(ExecuteArgs args)
        {
            await args.Connection.Send(new Send.InfoSend(args.Connection))
                .ConfigureAwait(false);
            // args.SendNotification(new Notification
            // {
            //     Image = "/img/svgrepo/essential-set-2/idea-138880.svg",
            //     Title = "Info requested",
            //     Description = "This is a test notification",
            //     HasProgress = true,
            //     Close = System.DateTime.UtcNow.AddSeconds(15)
            // });
        }

        public override void ReadJsonContent(JsonElement json)
        {
        }
    }
}