using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class FileDelete : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var id = Id.ToObjectId();
            if (id is null)
                return;
            
            var file = await args.Data.GetFileAsync(id, CancellationToken.None)
                .ConfigureAwait(false);
            if (file is null)
            {
                args.SendNotification(Notifications.Notification.Error(
                    "File not found",
                    ""
                ));
                return;
            }

            var stack = new Stack<Data.File>();
            stack.Push(file.Value.Value);
            var deleted = new List<Data.File>();

            while (stack.Count > 0)
            {
                var job = stack.Pop();

                await foreach (var child in file.Value.Source
                    .EnumerateFilesAsync(job.Person.Id, job.Id, CancellationToken.None)
                    .ConfigureAwait(false)
                )
                    stack.Push(child.Value);

                job.Deleted = true;
                await file.Value.Source.UpdateFile(job, CancellationToken.None)
                    .ConfigureAwait(false);
                deleted.Add(job);
            }

            file.Value.Value.Deleted = true;
            await file.Value.Source.UpdateFile(file.Value.Value, CancellationToken.None)
                .ConfigureAwait(false);
            
            if (deleted.Count == 1)
            {
                args.SendGlobalNotification(
                    new Notifications.Notification
                    {
                        Close = DateTime.UtcNow.AddSeconds(15),
                        Title = (file.Value.Value.Mime == "folder" ? "Folder" : "File")
                            + " deleted",
                        Description = file.Value.Value.Name,
                        Image = "/img/svgrepo/essential-set-2/garbage-51844.svg"
                    }
                );
            }
            else
            {
                args.SendGlobalNotification(
                    new Notifications.Notification
                    {
                        Close = DateTime.UtcNow.AddSeconds(15),
                        Title = deleted.Count.ToString() + " entries deleted",
                        Description = file.Value.Value.Name,
                        Image = "/img/svgrepo/essential-set-2/garbage-51844.svg"
                    }
                );
            }
            await WebSocketConnection.Foreach(async con =>
            {
                await Task.WhenAll(
                    deleted.Select(
                        entry => con.Send(new Events.Send.FileUpdated(
                            new DB.DBValue<Data.File>(file.Value.Source, entry)
                        ))
                    )
                ).ConfigureAwait(false);
            }).ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
        }
    }
}