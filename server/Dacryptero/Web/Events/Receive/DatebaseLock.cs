using System.Text.Json;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive;

public class DatabaseLock : ReceiveBase
{
    public string Database { get; private set; } = "";

    public override async Task Execute(ExecuteArgs args)
    {
        args.Connection.UnlockedDBs.TryRemove(Database, out _);
        await args.Connection.Send(new Send.InfoSend(args.Connection))
            .ConfigureAwait(false);
    }

    public override void ReadJsonContent(JsonElement json)
    {
        Database = json.GetProperty("database").GetString() ?? "";
    }
}