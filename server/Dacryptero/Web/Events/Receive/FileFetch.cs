using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class FileFetch : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var id = Id.ToObjectId();
            var file = id is not null ? 
                await args.Connection.DB.GetFileAsync(id, CancellationToken.None)
                    .ConfigureAwait(false) :
                null;
            await args.Connection.Send(new Send.FileSend(Id, file))
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
        }
    }
}