using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class PerformReindex : ReceiveBase
    {
        public string Database { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var db = args.Connection.Session.GetController(Database);
            if (db is null)
            {
                Serilog.Log.Debug("Database {name} not found", Database);
                return;
            }
            await db.RebuildIndex(false, args, CancellationToken.None)
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Database = json.GetProperty("database").GetString() ?? "";
        }
    }
}