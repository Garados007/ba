using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class DataInterviewAddRequest : ReceiveBase
    {
        public string Database { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var db = args.Connection.Session.GetController(Database);
            if (db is null)
                return;
            var interview = await db.AddNewInterviewAsync(CancellationToken.None)
                .ConfigureAwait(false);
            if (interview is null)
                return;
            await args.Connection.Send(new Send.DataInterviewAddSend(interview.Id))
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Database = json.GetProperty("database").GetString() ?? "";
        }
    }
}