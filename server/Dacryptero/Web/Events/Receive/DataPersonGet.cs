using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using LiteDB;

namespace Dacryptero.Web.Events.Receive
{
    public class DataPersonGet : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var id = Id.ToObjectId();
            DB.DBValue<Data.Person>? person = null;
            if (id != null)
            {
                person = await args.Data.GetPersonAsync(id, CancellationToken.None)
                    .ConfigureAwait(false);
            }
            await args.Connection.Send(new Send.DataPersonSend(Id, person))
                .ConfigureAwait(false);
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
        }
    }
}