using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web.Events.Receive
{
    public class DataInterviewListPersonRequest : ReceiveBase
    {
        public string Id { get; private set; } = "";

        public override async Task Execute(ExecuteArgs args)
        {
            var objId = Id.ToObjectId();
            if (objId is null)
                return;
            await foreach (var chunk in args.Data
                .EnumerateRawPeopleFromInterviewAsync(objId, CancellationToken.None)
                .Chunks(10, CancellationToken.None)
                .ConfigureAwait(false)
            )
            {
                await args.Connection.Send(new Send.DataInterviewListPersonSend(Id, chunk))
                    .ConfigureAwait(false);
            }
        }

        public override void ReadJsonContent(JsonElement json)
        {
            Id = json.GetProperty("id").GetString() ?? "";
        }
    }
}