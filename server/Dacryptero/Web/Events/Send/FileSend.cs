using System;
using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class FileSend : SendBase
    {
        public string Id { get; }

        public DB.DBValue<Data.File>? File { get; }

        public FileSend(string id, DB.DBValue<Data.File>? file)
        {
            Id = id;
            File = file;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id);
            if (File is null)
                writer.WriteNull("data");
            else
            {
                writer.WritePropertyName("data");
                File.Value.Value.WriteTo(writer, File.Value.Source);
            }
        }
    }
}