using System.Text.Json;
using Dacryptero.DB.Creation;

namespace Dacryptero.Web.Events.Send;

public class DatabaseAddRequestInfo : SendBase
{
    public NewDBInfo NewDBInfo { get; }

    public DatabaseAddRequestInfo(NewDBInfo info)
    {
        NewDBInfo = info;
    }

    protected override void WriteJsonContent(Utf8JsonWriter writer)
    {
        writer.WritePropertyName("info");
        NewDBInfo.WriteTo(writer);
    }
}