using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class SchemaResponse : SendBase
    {
        // private static JsonElement Schema = JsonDocument.Parse(@"
        //     [
        //         { 
        //             ""name"": ""Base"",
        //             ""abstract"": true,
        //             ""parent"": null,
        //             ""attributes"": {
        //                 ""age"": {
        //                     ""type"": ""Number"",
        //                     ""protected"": true,
        //                     ""history"": false
        //                 },
        //                 ""personal experience"": {
        //                     ""type"": ""MultiLine"",
        //                     ""protected"": false,
        //                     ""history"": true
        //                 }
        //             }
        //         },
        //         { 
        //             ""name"": ""Doctor"",
        //             ""abstract"": false,
        //             ""parent"": ""Base"",
        //             ""attributes"": {
        //                 ""occupation"": {
        //                     ""type"": ""SingleLine"",
        //                     ""protected"": false,
        //                     ""history"": true
        //                 }
        //             }
        //         },
        //         { 
        //             ""name"": ""Family"",
        //             ""abstract"": false,
        //             ""parent"": ""Base"",
        //             ""attributes"": {
        //                 ""personal view religion"": {
        //                     ""type"": ""MultiLine"",
        //                     ""protected"": false,
        //                     ""history"": true
        //                 }
        //             }
        //         }
        //     ]
        // ").RootElement;

        public string Name { get; }

        public Data.Schema.SchemaFile Schema { get;}

        public SchemaResponse(string name, Data.Schema.SchemaFile schema)
        {
            Name = name;
            Schema = schema;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("name", Name);
            writer.WritePropertyName("schema");
            // Schema.WriteTo(writer);
            Schema.WriteRolesTo(writer);
        }
    }
}