using System;
using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class DataPersonSend : SendBase
    {
        public string Id { get; }

        public DB.DBValue<Data.Person>? Person { get; }

        public DataPersonSend(string id, DB.DBValue<Data.Person>? person)
        {
            Id = id;
            Person = person;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id);
            if (Person is null)
            {
                writer.WriteNull("data");
            }
            else
            {
                writer.WritePropertyName("data");
                Person.Value.Value.WriteTo(writer, Person.Value.Source.Config.Name);
            }
        }
    }
}