using System.Text.Json;

namespace Dacryptero.Web.Events.Send;

public class LoginRequest : SendBase
{
    public string? Database { get; }

    public LoginRequest(string? database)
    {
        Database = database;
    }

    protected override void WriteJsonContent(Utf8JsonWriter writer)
    {
        writer.WriteString("database", Database);
    }
}