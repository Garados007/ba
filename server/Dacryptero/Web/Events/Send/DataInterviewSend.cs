using System.Text.Json;
using System;
using LiteDB;

namespace Dacryptero.Web.Events.Send
{
    public class DataInterviewSend : SendBase
    {
        public string Id { get; }

        public DB.DBValue<Data.Interview>? Interview { get; }

        public DataInterviewSend(string id, DB.DBValue<Data.Interview>? interview)
        {
            Id = id;
            Interview = interview;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id);
            if (Interview is null)
                writer.WriteNull("data");
            else
            {
                var interview = Interview.Value.Value;
                writer.WriteStartObject("data");
                writer.WriteString("id", interview.Id.ToString());
                writer.WriteString("database", Interview.Value.Source.Config.Name);
                writer.WriteString("created", interview.Created);
                writer.WriteString("modified", interview.Modified);
                writer.WriteBoolean("deleted", interview.Deleted);
                writer.WriteString("time", interview.Time);
                writer.WriteString("location", interview.Location);
                writer.WriteString("interviewer", interview.Interviewer);
                writer.WriteEndObject();
            }
        }
    }
}