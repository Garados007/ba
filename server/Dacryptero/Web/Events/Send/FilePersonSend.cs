using System;
using System.Linq;
using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class FilePersonSend : SendBase
    {
        public string Person { get; } = "";

        public string? Root { get; }

        public ReadOnlyMemory<DB.DBValue<Data.File>> Files { get; }

        public FilePersonSend(string person, string? root, ReadOnlyMemory<DB.DBValue<Data.File>> files)
        {
            Person = person;
            Root = root;
            Files = files;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("person", Person);
            if (Root is null)
                writer.WriteNull("root");
            else writer.WriteString("root", Root);

            writer.WriteStartArray("data");
            foreach (var file in Files.Span)
            {
                file.Value.WriteTo(writer, file.Source);
            }
            writer.WriteEndArray();
        }
    }
}