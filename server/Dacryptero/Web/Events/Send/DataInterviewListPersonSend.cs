using System;
using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class DataInterviewListPersonSend : SendBase
    {
        public string Id { get; }

        public ReadOnlyMemory<DB.DBValue<Data.Person>> People { get; }

        public DataInterviewListPersonSend(string id, ReadOnlyMemory<DB.DBValue<Data.Person>> people)
        {
            Id = id;
            People = people;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id);
            writer.WriteStartArray("persons");
            foreach (var entry in People.Span)
            {
                writer.WriteStartObject();
                writer.WriteString("id", entry.Value.Id.ToString());
                writer.WriteString("role", entry.Value.Role);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
        }
    }
}