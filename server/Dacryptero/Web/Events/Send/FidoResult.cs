using System.Text.Json;

namespace Dacryptero.Web.Events.Send;

public class FidoResult : SendBase
{
    public bool IsSuccess { get; }

    public string? Error { get; }

    public FidoResult()
    {
        IsSuccess = true;
        Error = null;
    }

    public FidoResult(string error)
    {
        IsSuccess = false;
        Error = error;
    }

    protected override void WriteJsonContent(Utf8JsonWriter writer)
    {
        writer.WriteBoolean("is-success", IsSuccess);
        writer.WriteString("error", Error);
    }
}