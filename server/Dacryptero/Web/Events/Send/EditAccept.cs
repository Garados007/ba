using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class EditAccept : SendBase
    {
        public string Id { get; }

        public string? Key { get; }

        public EditTarget Target { get; }

        public EditAccept(string id, string? key, EditTarget target)
        {
            Id = id;
            Key = key;
            Target = target;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            writer.WriteString("id", Id);
            if (Key is null)
                writer.WriteNull("key");
            else writer.WriteString("key", Key);
            writer.WriteString("target", Target.ToString());
        }
    }
}