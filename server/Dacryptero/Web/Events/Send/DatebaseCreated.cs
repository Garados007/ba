using System.Text.Json;

namespace Dacryptero.Web.Events.Send;

public class DatabaseCreated : SendBase
{
    public DB.Creation.NewDBInfo Info { get; }

    public DatabaseCreated(DB.Creation.NewDBInfo info)
    {
        Info = info;
    }

    protected override void WriteJsonContent(Utf8JsonWriter writer)
    {
        writer.WriteString("id", Info.Key);
    }
}