using System;
using System.Text.Json;

namespace Dacryptero.Web.Events.Send
{
    public class FileUpdated : SendBase
    {
        public DB.DBValue<Data.File> File { get; }

        public FileUpdated(DB.DBValue<Data.File> file)
        {
            File = file;
        }

        protected override void WriteJsonContent(Utf8JsonWriter writer)
        {
            File.Value.WriteTo(writer, File.Source, embeded: true);
        }
    }
}