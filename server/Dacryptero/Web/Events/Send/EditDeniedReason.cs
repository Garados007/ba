namespace Dacryptero.Web.Events.Send
{
    public enum EditDeniedReason
    {
        IdNotFound,
        SchemaNotFound,
        AttributeNotAvailable,
        AttributeForbidden,
        InvalidValueType,
        Generic,
    }
}