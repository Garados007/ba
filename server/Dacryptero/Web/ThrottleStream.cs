using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Dacryptero.Web
{
    public class ThrottleStream : Stream
    {
        public Stream BaseStream { get; }

        public TimeSpan Throttle { get; }

        public ThrottleStream(Stream stream, TimeSpan throttle)
        {
            BaseStream = stream;
            Throttle = throttle;
        }

        public override bool CanRead => BaseStream.CanRead;

        public override bool CanSeek => BaseStream.CanSeek;

        public override bool CanWrite => BaseStream.CanWrite;

        public override long Length => BaseStream.Length;

        public override long Position
        {
            get => BaseStream.Position;
            set => BaseStream.Position = value;
        }

        public override void Flush()
        {
            BaseStream.Flush();
        }

        public override Task FlushAsync(CancellationToken cancellationToken)
        {
            return BaseStream.FlushAsync(cancellationToken);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int read = BaseStream.Read(buffer, offset, count);
            Task.Delay(Throttle).Wait();
            return read;
        }

        public override int Read(Span<byte> buffer)
        {
            int read = base.Read(buffer);
            Task.Delay(Throttle).Wait();
            return read;
        }

        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            int read = await BaseStream.ReadAsync(buffer, offset, count, cancellationToken)
                .ConfigureAwait(false);
            await Task.Delay(Throttle, cancellationToken).ConfigureAwait(false);
            return read;
        }

        public override async ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = default)
        {
            var read = await base.ReadAsync(buffer, cancellationToken)
                .ConfigureAwait(false);
            await Task.Delay(Throttle, cancellationToken).ConfigureAwait(false);
            return read;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return BaseStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            BaseStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            BaseStream.Write(buffer, offset, count);
            Task.Delay(Throttle).Wait();
        }

        public override void Write(ReadOnlySpan<byte> buffer)
        {
            BaseStream.Write(buffer);
            Task.Delay(Throttle).Wait();
        }

        public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            await base.WriteAsync(buffer, offset, count, cancellationToken)
                .ConfigureAwait(false);
            await Task.Delay(Throttle, cancellationToken).ConfigureAwait(false);
        }

        public override async ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = default)
        {
            await base.WriteAsync(buffer, cancellationToken)
                .ConfigureAwait(false);
            await Task.Delay(Throttle, cancellationToken).ConfigureAwait(false);
        }
    }
}