using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MaxLib.WebServer;

namespace Dacryptero.Web
{
    public class StorageInvalidService : WebService
    {
        public StorageInvalidService()
            : base(ServerStage.CreateDocument)
        {
        }

        public override bool CanWorkWith(WebProgressTask task)
        {
            return task.Request.Location.IsUrl(new[] { "storage", "invalid" });
        }

        public override async Task ProgressTask(WebProgressTask task)
        {
            var param = task.Request.Location.GetParameter;
            task.Response.StatusCode = HttpStateCode.Forbidden;
            await Tools.AddTemplate(task, "storage-invalid.html",
                new Dictionary<string, string>
                {
                    { "NAME"
                    , param.TryGetValue("name", out string? name) 
                        ? System.Net.WebUtility.HtmlEncode(name)
                        : "The requested file" 
                    },
                }
            );
        }
    }
}