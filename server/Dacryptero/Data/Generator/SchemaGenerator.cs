using System;
using Dacryptero.Data.Schema;

namespace Dacryptero.Data.Generator
{
    public class SchemaGenerator
    {
        public SchemaFile GenerateFile(string localPath)
        {
            var file = new SchemaFile(localPath, "/internal/schemas/large-test.json", null);
            GenerateRolesForFile(file);
            file.UpdateCombinedRoles();
            return file;
        }

        private void GenerateRolesForFile(SchemaFile file)
        {
            // generate medium size role queue but everyone has attributes
            var parent = GenerateLongRoleQueue(file, "q1", null, 25);
            foreach (var r in file.Roles)
                GenerateRoleAttributes(r);
            var role = new SchemaRole("many attributes", false, parent);
            GenerateRoleAttributes(role);
            file.Roles.Add(role);
            // generate extremly long role queue, only the last entry will have attributes
            parent = GenerateLongRoleQueue(file, "q2", null, 1000);
            role = new SchemaRole("long queue", false, parent);
            GenerateRoleAttributes(role);
            file.Roles.Add(role);
        }

        private string GenerateLongRoleQueue(SchemaFile file, string prefix, string? root, int queueLength)
        {
            for (int i = 0; i < queueLength; ++i)
            {
                var name = $"{prefix}_r{i}";
                file.Roles.Add(
                    new SchemaRole(name, true, root)
                );
                root = name;
            }
            return root ?? $"{prefix}_r0";
        }

        private void GenerateRoleAttributes(SchemaRole role)
        {
            foreach (var value in Enum.GetValues<SchemaAttributeType>())
                role.Attributes.Add(
                    $"{role.Name}_a{value}",
                    new SchemaAttribute(
                        type: value,
                        @protected: false,
                        history: true
                    )
                );
            role.Attributes.Add(
                $"{role.Name}_b{SchemaAttributeType.MultiLine}",
                new SchemaAttribute(
                    type: SchemaAttributeType.MultiLine,
                    @protected: true,
                    history: true
                )
            );
        }
    }
}