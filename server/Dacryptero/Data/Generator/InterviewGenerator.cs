using System;
using System.Collections.Generic;

namespace Dacryptero.Data.Generator
{
    public class InterviewGenerator : DataGeneratorBase<Interview>
    {
        public InterviewGenerator(ReadOnlyMemory<WikiMarkow> wikiGen, NameGenerator nameGen,
            CityGenerator cityGen, Random? random = null
        )
            : base(wikiGen, nameGen, cityGen, random)
        {
        }

        public override Interview Generate()
        {
            var now = DateTime.UtcNow;
            var created = GetDateTime(now.AddYears(-10), now);
            return new Interview
            {
                Created = created,
                Modified = GetDateTime(created, now),
                Deleted = GetBool(0.05),
                Interviewer = GetName(),
                Time = GetDateTime(created.AddYears(-1), created.AddDays(-5)),
                Location = GetCity(),
            };
        }
    }
}