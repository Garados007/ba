using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using System.Text.RegularExpressions;

namespace Dacryptero.Data
{
    public enum IndexSource
    {
        Files,
        Interviews,
        Persons,
    }

    public class Index
    {
        static Index()
        {
            BsonMapper.Global.RegisterType<HashSet<ObjectId>>(
                serialize: SerializeHashSet,
                deserialize: DeserializeHashSet
            );
            BsonMapper.Global.RegisterType<Dictionary<ObjectId, int>>(
                serialize: SerializeObjDict,
                deserialize: DeserializeObjDict
            );
        }

        [BsonId]
        public ObjectId Id { get; set; } = new ObjectId();

        public string Token { get; set; } = "";

        public IndexSource Source { get; set; }

        public Dictionary<ObjectId, int> Refs { get; set; } = new Dictionary<ObjectId, int>();

        /// <summary>
        /// This is a list of all <see cref="Index.Id" /> which <see cref="Index.Token" /> contains
        /// this <see cref="Token" />.
        /// </summary>
        /// <typeparam name="ObjectId"></typeparam>
        /// <returns></returns>
        public HashSet<ObjectId> ChildTokens { get; set; } = new HashSet<ObjectId>();

        static readonly Regex tokenifier = new Regex(
            "[a-z0-9\\-]{3,}", 
            RegexOptions.IgnoreCase | RegexOptions.Compiled
        );

        public static IEnumerable<string> Tokenify(string source)
        {
            source = source.ToLowerInvariant();
            foreach (Match match in tokenifier.Matches(source))
            {
                yield return match.Value;
            }
        }

        public static IEnumerable<string> GetPartialTokens(string token, int minLength)
        {
            for (int i = minLength; i <= token.Length; ++i)
            {
                for (int j = 0; j + i <= token.Length; ++j)
                    yield return token[j .. (j + i)];
            }
        }

        public static BsonValue SerializeHashSet(HashSet<ObjectId> collection)
        {
            return new BsonArray(collection.Select(x => new BsonValue(x)));
        }

        public static HashSet<ObjectId> DeserializeHashSet(BsonValue value)
        {
            if (!value.IsArray)
                throw new NotSupportedException("item is not an array");
            return new HashSet<ObjectId>(value.AsArray.Select(x => x.AsObjectId));
        }

        public static BsonValue SerializeObjDict(Dictionary<ObjectId, int> dictionary)
        {
            return new BsonDocument(
                dictionary.ToDictionary(x => x.Key.ToString(), x => new BsonValue(x.Value))
            );
        }

        public static Dictionary<ObjectId, int> DeserializeObjDict(BsonValue value)
        {
            if (!value.IsDocument)
                throw new NotSupportedException("item is not a document");
            return value.AsDocument.ToDictionary(x => new ObjectId(x.Key), x => x.Value.AsInt32);
        }
    }
}