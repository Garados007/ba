using System;
using LiteDB;
using System.Text.Json;

namespace Dacryptero.Data
{
    public class AttributeEntry
    {
        [BsonId]
        public ObjectId Id { get; set; } = new ObjectId();

        public DateTime Date { get; set; }

        public BsonValue Value { get; set; } = BsonValue.Null;

        public void WriteTo(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteString("date", Date);
            writer.WriteBson("value", Value);
            writer.WriteEndObject();
        }
    }
}