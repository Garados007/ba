using System;
using System.Collections.Generic;
using System.Text.Json;
using LiteDB;

namespace Dacryptero.Data
{
    public class Attribute : Base
    {
        public ObjectId PersonId { get; set; } = ObjectId.Empty;

        public string Key { get; set; } = "";

        [BsonRef("attribute_entry")]
        public List<AttributeEntry> Entries { get; set; } = new List<AttributeEntry>();

        [BsonRef("attribute_entry")]
        public AttributeEntry RecentEntry { get; set; } = new AttributeEntry();

        public void WriteTo(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteString("key", Key);
            writer.WriteString("created", Created);
            writer.WriteString("modified", Modified);
            writer.WriteStartArray("entries");
            foreach (var entry in Entries)
                entry.WriteTo(writer);
            writer.WriteEndArray();
            writer.WritePropertyName("entry");
            RecentEntry.WriteTo(writer);
            writer.WriteEndObject();
        }
    }
}