using System;
using System.Collections.Generic;
using LiteDB;
using Dacryptero.Search;
using Dacryptero.Notifications;

namespace Dacryptero.Data
{
    public class Interview : Base, ISearchable
    {
        public bool Deleted { get; set; }

        public DateTime Time { get; set; }

        public string Location { get; set; } = "";

        public string Interviewer { get; set; } = "";

        public List<ObjectId> Person { get; set; } = new List<ObjectId>();

        [BsonIgnore]
        public SearchResultType ResultType => SearchResultType.Interview;

        public List<(string name, string value)> GetInfo()
        {
            var list = new List<(string name, string value)>
            {
                ("created", Created.ToString("s")),
                ("modified", Modified.ToString("s")),
                ("time", Time.ToString("s")),
                ("people", Person.Count.ToString()),
            };
            if (Deleted)
                list.Insert(0, ("deleted", "true"));
            return list;
        }

        public string GetName() => $"{Location}, with {Interviewer}";

        public string? GetRole() => null;

        public IEnumerable<SearchPart> GetSearchParts()
        {
            yield return new SearchPart(FoundType.Field, "created", new BsonValue(Created));
            yield return new SearchPart(FoundType.Field, "modified", new BsonValue(Modified));
            yield return new SearchPart(FoundType.Field, "deleted", new BsonValue(Deleted));
            yield return new SearchPart(FoundType.Field, "time", new BsonValue(Time));
            yield return new SearchPart(FoundType.Field, "location", new BsonValue(Location));
            yield return new SearchPart(FoundType.Field, "interviewer", new BsonValue(Interviewer));
        }

        public void PerformIndex(DB.DBSingleController db, bool remove, ITransmitter? transmitter)
        {
            using var confLock = db.GetIndexLock(true, transmitter);

            db.SetIndex(IndexSource.Interviews, Id, Location, true, remove, transmitter);
            db.SetIndex(IndexSource.Interviews, Id, Interviewer, true, remove, transmitter);

            GC.KeepAlive(confLock);
        }
    }
}