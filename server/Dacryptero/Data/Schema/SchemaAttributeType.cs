namespace Dacryptero.Data.Schema
{
    public enum SchemaAttributeType
    {
        DateTime,
        SingleLine,
        MultiLine,
        Number,
        Int,
        Bool,
    }
}