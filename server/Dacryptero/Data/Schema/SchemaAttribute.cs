using System;
using System.Text.Json;

namespace Dacryptero.Data.Schema
{
    public class SchemaAttribute
    {
        public SchemaAttributeType Type { get; }

        public bool Protected { get; }

        public bool History { get; }

        public SchemaAttribute(SchemaAttributeType type, bool @protected, bool history)
        {
            Type = type;
            Protected = @protected;
            History = history;
        }

        public SchemaAttribute(JsonElement json)
        {
            Type = Enum.Parse<SchemaAttributeType>(
                json.GetProperty("type").GetString() ?? "",
                true
            );
            Protected = json.GetProperty("protected").GetBoolean();
            History = json.GetProperty("history").GetBoolean();
        }

        public void WriteTo(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteString("type", Type.ToString());
            writer.WriteBoolean("protected", Protected);
            writer.WriteBoolean("history", History);
            writer.WriteEndObject();
        }
    }
}