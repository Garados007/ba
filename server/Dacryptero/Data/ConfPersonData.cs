using System.Collections.Generic;
using Dacryptero.Search;
using LiteDB;

namespace Dacryptero.Data
{
    public class ConfPersonData
    {    
        [BsonId]
        public ObjectId Id { get; set; } = new ObjectId();

        [BsonRef("interview")]
        public Interview Interview { get; set; } = new Interview();

        public string Name { get; set; } = "";

        public string Contact { get; set; } = "";

        [BsonRef("attribute")]
        public List<Attribute> Attributes { get; set; } = new List<Attribute>();

        public IEnumerable<SearchPart> GetSearchParts()
        {
            yield return new SearchPart(FoundType.Field, "name", new BsonValue(Name));
            yield return new SearchPart(FoundType.Field, "contact", new BsonValue(Contact));
        }
    }
}