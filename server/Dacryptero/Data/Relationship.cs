using LiteDB;

namespace Dacryptero.Data
{
    public class Relationship
    {
        public ObjectId Id { get; set; } = ObjectId.Empty;

        [BsonRef("person")]
        public Person Person1 { get; set; } = new Person();

        [BsonRef("person")]
        public Person Person2 { get; set; } = new Person();

        public string Relation { get; set; } = "";
    }
}