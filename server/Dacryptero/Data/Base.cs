using System;
using LiteDB;

namespace Dacryptero.Data
{
    public abstract class Base
    {
        [BsonId]
        public ObjectId Id { get; set; } = ObjectId.Empty;

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}