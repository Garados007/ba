#!/bin/bash

function create_dir () {
    if [ ! -d "$1" ]; then
        mkdir -p "$1"
    fi
}

create_dir "$HOME/Documents/Dacryptero/config"
create_dir "$HOME/Documents/Dacryptero/db"
create_dir "$HOME/Documents/Dacryptero/logs"

pushd bin
dotnet Dacryptero.dll --config ../config-demo-linux.ini
popd
