@echo off

if not exist "%APPDATA%\Dacryptero" (
    mkdir "%APPDATA%\Dacryptero"
)
if not exist "%APPDATA%\Dacryptero\config" (
    mkdir "%APPDATA%\Dacryptero\config"
)
if not exist "%APPDATA%\Dacryptero\db" (
    mkdir "%APPDATA%\Dacryptero\db"
)
if not exist "%APPDATA%\Dacryptero\logs" (
    mkdir "%APPDATA%\Dacryptero\logs"
)

pushd bin
dotnet Dacryptero.dll --config ../config-demo-windows.ini
popd
