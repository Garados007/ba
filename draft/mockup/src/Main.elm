module Main exposing (..)

import Html exposing (Html,div,text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Browser
import Json.Encode as JE
import Views.AddEntry
import Views.Search

main : Program () Model Msg
main =
    Browser.document
        { init = \() -> init
        , view = \model ->
            { title = "Mockup"
            , body = view model
            }
        , update = update
        , subscriptions = \_ -> Sub.none
        }

type alias Model = ViewMode
type Msg
    = SetViewMode ViewMode

type ViewMode
    = ViewAddEntry
    | ViewSearch

init : (Model, Cmd Msg)
init = (ViewSearch, Cmd.none)

view : Model -> List (Html Msg)
view model =
    [ Html.node "link"
        [ HA.property "rel" <| JE.string "stylesheet"
        , HA.href "style.css"
        ] []
    , div [ class "header-line" ]
        [ div [ class "title" ]
            [ text "Mockup" ]
        ]
    , div [ class "main-content" ]
        [ viewLeftMenu model
        , div [ class "content" ]
            <| List.singleton
            <| case model of
                ViewAddEntry -> Views.AddEntry.view model
                ViewSearch -> Views.Search.view model
        ]
    ]

viewLeftMenu : Model -> Html Msg
viewLeftMenu _ =
    div [ class "left-menu" ]
    <| List.map
        (\(mode, title) ->
            div 
                [ class "button"
                , HE.onClick <| SetViewMode mode
                ]
                [ text title ]
        )
        [ (ViewAddEntry, "Add Entry")
        , (ViewSearch, "Search")
        ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg _ =
    case msg of
        SetViewMode mode -> (mode, Cmd.none)
