module Views.Search exposing (..)

import Html exposing (Html,div,text)
import Html.Attributes as HA exposing (class)

type alias SearchResult =
    { kind: String
    , name: String
    , info: List (String, String)
    , attribute: String
    , result: List (String, Bool)
    }

viewResult : SearchResult -> Html msg
viewResult { kind, name, info, attribute, result } =
    div [ class "search-result" ]
        [ div [ class "title" ]
            [ div [ class "kind" ]
                [ text kind ]
            , div [ class "name" ]
                [ text name ]
            ]
        , div [ class "info" ]
            <| List.map
                (\(key, value) ->
                    div [ class "entry "]
                        [ div [ class "key" ]
                            [ text key ]
                        , div [ class "value" ]
                            [ text value ]
                        ]
                )
            <| info
        , div [ class "result" ]
            [ div [ class "attribute" ]
                [ text attribute ]
            , div [ class "found" ]
                <| List.map
                    (\(value, highlight) ->
                        Html.span
                            [ HA.classList
                                [ ("highlight", highlight)]
                            ]
                            [ text value ]
                    )
                <| result
            ]
        ]

view : model -> Html msg
view _ =
    div [ class "view-search" ]
        [ div [ class "search-bar" ]
            [ Html.input
                [ HA.value "london"
                ] []
            ]
        , viewResult 
            { kind = "interview"
            , name = "The Royal London Hospital For Integrated Medicine"
            , info =
                [ ("time", "2024-05-17 17:54")
                , ("interviewer", "Ralf Muster")
                , ("interviewee", "Max Mustermann (and 2 more)")
                ]
            , attribute = "location"
            , result =
                [ ("The Royal ", False)
                , ("London", True)
                , (" Hospital For Integrated...", False)
                ]
            }
        , viewResult 
            { kind = "Doctor"
            , name = "Max Mustermann"
            , info =
                [ ("location", "London"), ("occupation", "psychiatrist") ]
            , attribute = "location"
            , result =
                [ ("London", True)
                ]
            }
        , viewResult 
            { kind = "Doctor"
            , name = "Max Mustermann"
            , info =
                [ ("location", "London"), ("occupation", "psychiatrist") ]
            , attribute = "contact"
            , result =
                [ ("...Ormond Street, ", False)
                , ("London", True)
                , (", WC1N 3HR...", False)
                ]
            }
        ]