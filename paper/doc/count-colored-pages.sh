#!/bin/bash

colored="$(gs -o - -sDEVICE=inkcov bin/main.pdf | grep -v "^ 0.00000  0.00000  0.00000" | grep "^ " | wc -l)"
total="$(pdfinfo bin/main.pdf | grep Pages: | grep -Po "[0-9]+")"
echo "$colored/$total"
