## Installation

Dies sind die Schritte, die notwendig sind, damit das Produkt (die Anwendung) auf dem Rechner des
Nutzers läuft. Dafür gibt es verschiedene Methoden, die auch im Laufe der Entwicklung für diese
Arbeit durchgelaufen sind. Vom Prinzip her bauen diese aufeinander auf und nehmen immer mehr
manuelle Arbeit ab.

### Manuelle Installation

Am Anfang der Entwicklung wurde alles manuell installiert. Dies hat den Vorteil, dass der Entwickler
direkt sehen kann, was wie und wo benötigt wird. Außerdem erleichtert dies die Konfiguration
selbst. Es ist von Vorteil, sich diese Schritte irgendwie zu notieren, da dies für die spätere
Automatisierung benötigt wird.

Bei diesem Produkt bedeutete dies, dass folgende Produkte installieren bzw. Schritte durchgeführt
werden müssen:

1. Herunterladen des Quellcodes in einen beliebigen temporären Ordner
2. Installation des Elm Compilers
3. Kompilieren des Elm Codes
4. Installation von Javascript Komprimierungswerkzeugen
5. Komprimierung des Javascript Codes
6. Installation von .NET SDK
7. Kompilieren des Server C# Codes
8. Kompilieren von Server Tools und Ausführung dieser
9. Anlegen der Programmverzeichnisstruktur auf dem Zielrechner
10. Kopieren der kompilierten Server- und Javascript-Dateien in die Programmverzeichnisstruktur
11. Kopieren der statischen Inhalte für die Web-Oberfläche in die Programmverzeichnisstruktur
12. Anlegen der Konfigurationsdatei
13. Anlegen der Verknüpfung zum starten der Anwendung

Diese Schritte sind vom Prinzip her unter Windows und Linux gleich, auch wenn diese sich im Detail
leicht unterscheiden (z. B. Installationsort des Programms).

### Halbautomatische Installation mit Docker und CI Pipeline

Einige Schritte aus der manuellen Installation, welche sich vereinfachen lassen, werden schon im
Vorfeld kompiliert, um sie dann fertig auf den Zielrechner runterzuladen. Dazu eignet sich eine
CI-Pipeline, so wie sie auch in dieser Arbeit genutzt wurde. Das ist ein spezielles Script, welches
von einem Server gestartet wird, wenn ein Entwickler neuen Code in die Codeverwaltung (in diesem
Fall GitLab, geht, aber auch mit anderen wie GitHub) hochlädt.

Der Server startet dann verschiedene Dockercontainer, was in sich abgeschlossene und konsistente
Umgebungen sind, und führt darin vordefinierte Befehle aus. Das Ziel von Dockercontainern ist, dass
immer die gleichen Bedingungen (installierte Software, Konfiguration, etc.) herrschen und daher
genau ersichtlich ist, was genau getan werden muss.

Die Befehle der Schritte 1 bis 8 aus \ref{071-manuelle-installation} werden zusammengefasst, und
dadurch fallen diese Schritte auch bei der Installation beim Nutzer weg, da das Ergebnis schon
fertig auf dem Server existiert. Dafür werden diese 8 Schritte beim Nutzer durch den Download der
fertig gebauten Sachen und Installation von .NET Runtime (schmalere Version von .NET SDK) ersetzt.
Auf dem Server kommt noch hinzu, dass alle notwendigen Dateien noch einmal zusammengefasst werden,
damit sie besser für die Installation geeignet sind.

Einen weiteren Vorteil hat diese Vorgehensweise auch. So ist relativ früh erkenntlich, ob es
Probleme beim Kompilieren und Zusammenstellen gibt und das, bevor die Installation beim Nutzer
durchführt wird. Außerdem lässt sich auf dem Server noch automatische Tests ausführen (siehe
\ref{061-automatisierte-tests}) und die Versionierung erleichtern.

### Vollautomatische Installation mit Wix (Windows)

An Automatisierung fehlen nur noch die letzten Schritte 9 bis 13 aus
\ref{071-manuelle-installation}. Unter Windows eignet sich das von Microsoft veröffentlichte
Softwaretool Wix. Hiermit wird eine XML-Datei angelegt, die alle Anweisungen enthält, die für die
Installation notwendig sind. Danach gibt es einen Compiler, der die XML-Datei mit Anweisungen und
alle zu installierenden Dateien einliest, zusammenpackt und eine ausführbare EXE- oder MSI-Datei
erstellt.

Diese Schritte lassen sich auch automatisch auf dem Server in einen Dockercontainer ausführen,
sodass am Ende nur noch die EXE- oder MSI-Datei übrig bleibt. Daher lässt sich die
Installationsroutine am Nutzer so zusammenfassen:

1. Installer herunterladen
2. Installer starten und abwarten. Eventuell Konfiguration vornehmen
3. Fertig

Dies wurde für diese Arbeit nicht fertiggestellt und ist daher ein guter Punkt für zukünftige
Erweiterungen.

### Vollautomatische Installation unter Linux

Genauso wie sich die Installation am Nutzer bei Windows zusammenfassen lässt, geht dies auch unter
Linux nur mit anderen Mitteln. Unter Linux gibt es eine große Palette an Werkzeugen, da je nach
Linux Distribution einige Sachen anders funktionieren. So ist z. B. die Paketverwaltung (wird
benötigt, um Abhängigkeiten zu installieren) bei einem Debian-Linux `apt` und bei einem Arch-Linux
`pacman`, welche natürlich jeweils andere Formate sehen wollen.

Als Entwickler lässt sich dies vereinfachen, indem dieser sich ein Shellskript schreibt, was alle
Installationsanweisungen enthält und dieses im Detail nachschaut, unter welcher Distribution es sich
derzeit befindet.

Dies bedeutet natürlich aber auch viel Arbeit und das wurde aus Zeit- und Prioritätsgründen nicht
vom Autor praktisch umgesetzt.
