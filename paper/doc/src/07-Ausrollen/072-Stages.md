## Stages

Von der Entwicklung des Codes bis zum Nutzer durchlaufen Änderungen an der Codebasis verschiedene
Stages (Stadien). Üblicherweise wird hier mit einem Modell gearbeitet, was drei (Entwicklung,
Staging, Produktiv) oder vier (Entwicklung, Test, Staging, Produktiv) Stages enthält.

Die Entwicklungsstage findet direkt beim Entwickler statt. Die Anwendung und ihre Daten können
jederzeit vom Entwickler kaputtgemacht und wieder komplett neu aufgebaut werden. Hier kann und darf
es passieren, dass sämtliche Nutzerdaten zerstört werden.

Danach geht es in die Test-Stage, bei der geschaut wird, ob das ganze Produkt mit allen Änderungen
noch funktioniert und ob es Fehler gibt. Falls hier Probleme auftreten, dann werden diese direkt
zurück zum Entwickler kommuniziert. Üblicherweise wird hier mit verschiedenen Daten gearbeitet, die
Realdaten sehr ähnelt sollen, um sämtliche Szenarien besser abbilden zu können.

Danach geht es in die Stagingstage, die Änderungen enthält, die kurz vor Veröffentlichung stehen.
Hier können experimentierfreudige Nutzer die nächste Version ausprobieren.

Und schlussendlich kommen die Änderung in die Produktiv-Stage, in der diese dann direkt bei allen
Nutzern installiert werden. Hier sollten keine Fehler mehr in den Änderungen existieren, da hier mit
realen Nutzerdaten gearbeitet wird, die nicht verloren gehen dürfen.

Der Autor hat sich für seine Entwicklung für das 3-Stage-Modell entschieden, da die Entwicklung noch
am Anfang ist und die zusätzliche Test-Stage erhöhten Aufwand bedeutet. Die Test-Stage kann
jederzeit nachträglich eingeführt werden.

Die 3 Stage spiegeln sich auch in der Quellcode-Organisation des Projekts wider. Die
Entwicklungsstage sind sämtliche Feature- oder Fix-Branche, die der Autor in seiner Entwicklung
anlegt. Da drin kann und darf alles passieren. Sobald die Änderungen an einem Branch abgeschlossen
und in sich getestet sind, werden diese in den `develop`-Branch überführt. Dies entspricht derzeit
der Staging-Stage. Hier wird alles insgesamt noch mal getestet. Oftmals mehrere Änderungen aus der
Entwicklungsstage gleichzeitig. Nachdem alle Änderungen hier bestanden haben, werden diese in den
`master`-Branch (Produktiv-Stage) überführt und eine neue Versionsnummer wird erstellt.
