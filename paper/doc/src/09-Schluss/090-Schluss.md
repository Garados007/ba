\cleardoublepage

# Abschlussbetrachtung

Der Kern am Projekt war das Sicherheitskonzept und die Wahrung des Datenschutzes, damit alle Daten
in jeglicher Situation geschützt sind. Um dieses Konzept herum wurde eine Datenbankanwendung mit
modernen Prinzipien und Techniken entworfen, die das erstellte Sicherheitskonzept nutzen und
unterstützen.

Dabei hat sich schnell herausgestellt, dass es sehr wohl möglich ist, alle Anforderungen an die
Sicherheit zu befolgen und gleichzeitig eine praktische Anwendung zu entwerfen, die ein Forscher bei
seiner Arbeit unterstützen kann.

Leider wurde in der Analyse festgestellt, dass viele Datenbankmanagementsysteme sich nur sehr
schwierig so absichern lassen, dass all ihre Daten auch sicher verschlüsselt auf der Festplatte
gespeichert sind. Dass dies aber dennoch geht, wurde am Beispiel LiteDB sehr gut gezeigt.

Bei den Programmiersprachen kann der Entwickler relativ flexibel sein. Die meisten der betrachteten
sind ähnlich gut für die Umsetzung des Projekts geeignet. Trotzdem kann die Auswahl einen großen
Einfluss auf das Testen und somit die Entwicklung haben, da hier je nach Sprache bestimmte Fehler
leichter entdeckt oder ausgeschlossen werden.

Es gibt zwar viele Möglichkeiten für eine Zwei-Faktor-Authentifizierung, aber nur wenige, die auch
lokal, ohne Internetverbindung und externe Dienstleister funktionieren. In dieser Arbeit wurde ein
Hardwaretoken genutzt, welcher das Fido2-Protokoll benutzt. Leider ist dies noch im Entwurfsstadium
und noch nicht so stark verbreitet, dass nur die wenigsten Nutzer einen zu Hause haben und auch nur
große Webseiten bieten derzeit eine Unterstützung dafür an.

Ein weiteres, aber großes Problem für diese Arbeit war der zeitliche Aspekt. Es wurden sehr viele
Funktionen für die Anwendung angedacht, sodass es zum einen für den Autor schwierig war, diese alle
einzubauen, aber auch zum anderen diese zu dokumentieren. So bleibt zum Schluss eine große Liste an
Funktionen für künftige Erweiterungen, die noch ausgearbeitet und in die Anwendung eingebaut werden
können.

Abschließend lässt sich sagen, dass das Ziel, die Erleichterung der Arbeit mit Interviewdaten von
einer Feldforschung und die sichere Aufbewahrung dieser für Forscher an einem ethnologischen
Institut erreicht wurde.
