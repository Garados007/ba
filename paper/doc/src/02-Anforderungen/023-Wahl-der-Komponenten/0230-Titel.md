## Wahl der Komponenten

Für die jeweiligen Komponenten stehen verschiedene Optionen zur Verfügung. Hier wird für jede
Komponente verglichen und ausgewählt, welche Option genutzt wird.
