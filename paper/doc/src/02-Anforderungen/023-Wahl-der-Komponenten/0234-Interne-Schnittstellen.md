### Interne Schnittstellen

Zwischen den einzelnen Modulen gibt es interne Schnittstellen, damit diese kommunizieren können.
Zwischen Datenbank und Server wird dies über die verwendete Bibliothek geregelt und muss daher nicht
weiter berücksichtigt werden. Zwischen FIDO Key und Web-Oberfläche wird das hauptsächlich vom
Browser übernommen. Übrig bleibt nur noch die Schnittstelle zwischen dem Server und der
Web-Oberfläche. Hierfür gibt es die Bedingung, dass die Schnittstelle über einen herkömmlichen
Browser erreichbar, leicht erweiterbar und leicht verständlich sein soll.

Für den Großteil der Kommunikation wird eine WebSocket-Schnittstelle gewählt. Dazu wird ein
Nachrichtentunnel zwischen Oberfläche und Server aufgebaut, in denen verschiedene JSON formatierte
Nachrichtenpakete hin und her verschickt werden können. Dieses Protokoll ist nicht zustandsbasiert
und die Kommunikation kann jederzeit in beide Richtungen erfolgen. Weiterhin ist der Tunnel zwischen
beiden Teilnehmern sicher. Es kann sich keine dritte Partei (außer dem Webbrowser, da dieser ein
Vermittler zwischen Weboberfläche und Server ist) einmischen und zur Laufzeit der Verbindung, was
meist über die gesamte Dauer der Ausführung der Anwendung hinweg geht, sind beide Teilnehmer immer
authentifiziert.

Über diese WebSocket-Schnittstelle werden so gut wie alle Nachrichten übermittelt. Das hat den
Vorteil, dass beide Seiten sofort auf Ereignisse reagieren können.

Des Weiteren gibt es eine kleine REST-Schnittstelle. Hier werden über verschiedene URLs
hauptsächlich Dateien angeboten, da sie meist zu groß sind, um sie über WebSocket zu übertragen.
Damit wird versucht die Latenz über WebSocket möglichst gering zu halten und wichtigen Pakete
jederzeit den Vorrang geben zu können.

Ein Problem besteht bei der REST-Schnittstelle, da bei jedem Aufruf eine neue Verbindung aufgebaut
wird. Dadurch ist eine Authentifizierung nicht durchgehend möglich. Dafür wird ein kurzlebiger
Authentifizierungstoken über die WebSocket-Schnittstelle mitgeteilt, den die Web-Oberfläche nutzen
kann, um die REST Anfragen authentifizieren zu können.

Dieses Modell wurde an der üblichen Cookie-Authentifizierung abgeschaut, die bei sehr vielen
Webseiten zum Einsatz kommt. Auch hier wird einmal authentifiziert und dann wird ein Token als
kurzlebiger Cookie (Session-Cookie) immer wieder von der Oberfläche an dem Server weitergereicht.
Für dieses Projekt hat sich der Autor gegen einen Session-Cookie entschieden, da er die
WebSocket-Verbindung als Basis für die Authentifizierung nutzen wollte.
