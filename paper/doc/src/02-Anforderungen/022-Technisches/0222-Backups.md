### Back-ups

Es ist erforderlich, in regelmäßigen Abständen Back-ups von den Daten erstellen zu können. Dies
beinhaltet die komplette Datenbank inklusiver Metadaten, damit bei einem Ausfall, Verlust, etc.
die Daten leicht wiederhergestellt und daran weitergearbeitet werden kann.

Hierzu ist die Nutzung eines Cloud-Speicher-Dienstes geplant, welcher die Daten aus einem lokalen
Ordner automatisch mit der Cloud synchronisiert. Eine Herausforderung hierbei ist, dass es zu
Problemen mit der Synchronisierung kommen kann, wenn die gleiche Datenbank auf zwei Geräten offen
ist und über die gleiche Cloud synchronisiert wird. Hier kann es zu Kollisionen kommen, welche sich
nur schwer beheben lassen. Das ist vor allem deshalb der Fall, weil die Daten nur verschlüsselt und
binär vorliegen und sich daher eher schlecht vergleichen lassen.
