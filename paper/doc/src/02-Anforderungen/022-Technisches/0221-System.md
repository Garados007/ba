### System

An dem technischen System werden bedingt durch das Nutzungsszenario eine große Vielfalt an
Bedingungen gestellt, die es erfüllen muss:

1. Die Forscher sollen die Anwendung auf ihren Forschungsreisen nutzen, weswegen es vorkommen kann,
   dass es dabei zeitweise keinen Internetzugriff gibt. Daher müssen alle Daten offline verfügbar
   sein. Dennoch können online Back-ups erstellt werden, sobald eine Internetverbindung wieder
   besteht.
2. Es ist von sehr großen Datenmengen auszugehen. Die Forscher werden auf ihren Reisen mehrere
   hundert bis tausend Bildern und Videos aufnehmen. Daher muss das Zielgerät entsprechend
   Speicherplatz für die Anwendung bereitstellen. Für die externen Geräte (z. B. Kamera) müssen
   außerdem Anschlüsse vorhanden sein, um Daten übertragen zu können.
3. Des Weiteren muss für den Verschlüsselungsprozess auch die benötigte Rechenleistung zur Verfügung
   stehen.
4. Der Forscher wird mit vielen Daten gleichzeitig arbeiten müssen. Daher ist eine Oberfläche
   erforderlich, die dies einfach und effizient ermöglicht.
5. Die Geräte müssen relativ kostengünstig sein, da diese auf Forschungsreisen kaputt gehen,
   verloren oder gestohlen werden können und daher leicht zu ersetzen sein müssen.
6. Die Geräte sollten dem Forscher vertraut sein, damit sie den Arbeitsfluss erleichtert.

Aus diesen Anforderungen ergibt sich nach aktuellem Stand ein günstiger Laptop. Ein Mobiltelefon,
welches derzeit eine größere Verbreitung als ein Laptop gefunden hat, kommt aus folgenden Punkten
leider derzeit nicht infrage:

1. Die 2. Bedingung kann bei vielen Mobiltelefonen nur bedingt erfüllt werden. Es werden große
   Datenmengen von deutlich mehreren GB (hauptsächlich durch Bilder und Videos) erwartet, welche auf
   den begrenzten internen Speicher nur schlecht gespeichert werden können. Es gibt zwar
   Möglichkeiten der SD-Karten Erweiterung, welche aber immer seltener werden. Stattdessen bieten
   Hersteller immer größeren internen Speicher zu einen schlechterem Preis-Leistungsverhältnis an.
2. Da hier günstige Geräte erwartet werden, kann die benötigte Rechenleistung nur bedingt
   bereitgestellt werden. Zwar sind einzelne Ver- und Entschlüsselung im Hinblick auf die Rechenzeit
   relativ günstig, dafür aber viel aufwendiger, wen von mehreren Hundert Megabyte bis Gigabyte
   geredet wird. Dies ist besonders bei der Arbeit mit einer verschlüsselten Datenbank der Fall.
3. Mobiltelefone haben relativ kleine Oberflächen, wodurch die Übersichtlichkeit stark eingeschränkt
   wird. Außerdem ist die Arbeit mit der virtuellen Tastatur langsamer als mit einer realen. Zwar
   ist es hier möglich externes Zubehör zur Verfügung zu stellen (Maus, Tastatur und Bildschirm über
   spezielle USB-Hubs oder Bluetooth), welche aber die Kompaktheit reduzieren und auch wieder Geld
   kosten.

Zwar lassen sich die oberen drei Gegenargumente leicht widerlegen, indem dafür spezielle Systeme und
Oberflächen erstellt werden, die dafür speziell ausgelegt sind, das erhöht aber nur den Umfang der
Arbeit enorm. Dies kann aber eine Möglichkeit der zukünftigen Fortentwicklung sein.

Es gäbe noch die Möglichkeit, neben der Bereitstellung auf einem Laptop oder Mobiltelefon dies auch
als Web-App (eigenständige Anwendung im Webbrowser) bereitzustellen. Dies beinhaltet leider das
Problem, dass die Daten auch offline verfügbar sein müssen. Zwar ist es hier möglich, den
Browser-Cache und -Speicher zu nutzen, um bestimmte Daten zwischenzuspeichern, dieser ist aber
leider in seiner maximalen Größe sehr stark begrenzt und es ist nicht möglich die komplette
Datenbank dort unterzubekommen. Aufgrund der Komplexität wird diese Möglichkeit daher derzeit nicht
in Betracht gezogen.

Das Max-Planck-Institut für ethnologische Forschung stellt seit Jahren seinen Forschern
Windows-Laptops zur Verfügung, wenn diese sich auf eine Forschungsreise begeben. Nach dem aktuellen
Mobile-Device-Management-Plan sollen iPhone-Mobiltelefonie zum Reporteau hinzugefügt werden. Aus
oben genannten Gründen werden diese iPhones vorerst nicht berücksichtigt. Das primäre
Entwicklungsziel ist daher ein Laptop mit dem Betriebssystem Windows.
