\cleardoublepage

# Anforderungen

In den Gesprächen mit den Forschern und dem Auftraggeber wurde eine Liste an Anforderungen erstellt,
die für das Projekt zu erfüllen sind. Dazu gehören inhaltliche Anforderungen, welche besagen, was
die Anwendung für den Forscher primär leisten soll. Aber auch strukturelle Anforderungen, wie mit
dem Daten, die der Forscher bei seinen Forschungen erstellt oder welche bei der Arbeit mit dem
Programm entstehen, verarbeitet und gespeichert werden sollen. Weiterhin müssen auch technische
Anforderungen über den Aufbau der Anwendung, die Speicherdauer und Architektur erfüllt werden.

