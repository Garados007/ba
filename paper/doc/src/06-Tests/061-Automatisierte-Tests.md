## Automatisierte Tests

Ein großer Vorteil an den automatisierten Tests ist, dass die ohne menschliches Zutun gestartet,
durchgeführt und ausgewertet werden können. Dadurch lassen diese sich gut in den
Entwicklungsprozess einbinden, da diese regelmäßig mit den neuesten Änderungen am Code ausgeführt
werden. Das hat zur Folge, dass Probleme am Code relativ frühzeitig erkannt und somit auch behoben
werden können.

Je nachdem, ob nun das Front-End, Back-End oder die API testen werden sollen, gibt es
unterschiedliche Testarten, die sich mehr oder weniger gut dafür eignen. Aus zeitlichen Gründen
wurde bei vielen darauf verzichtet, diese im Projekt umzusetzen. Stattdessen wurden die manuell vom
Entwickler durchgeführt. Daher wird nur auf die Testarten genauer eingegangen, die auch ihre
Anwendung gefunden haben.

### Kompilierung

Dies ist ein relativ einfacher Test, da nur geschaut wird, ob der Compiler syntaktische Fehler im
Quellcode findet. Zum Teil kann der Compiler noch auf andere Fehlerquellen wie zum Beispiel
Typfehler untersuchen. In der Ausführung wird der Compiler auf dem Quellcode gestartet und
geschaut, ob es Fehler oder Warnungen gibt.

Die Compiler von den verwendeten Programmiersprachen C# und Elm sind sehr gut im Finden vieler
Fehler und geben hilfreiche Fehlermeldungen aus. Übrig bleiben hauptsächlich logische Fehler, die
sich dann manuell oder mit anderen Testarten testen lassen.

Bei diesem Projekt wurden alles korrigiert, was der Compiler als Fehler oder Warnung gemeldet hat.

### GitLab CI Tests

GitLab bietet verschiedene automatische Tests an, die mit wenigen Zeilen zur CI Pipeline hinzugefügt
werden können. Genutzt wurden dazu folgende Tests:

- **Static Application Security Testing (SAST)** (Quelle: \cite{gitlab-sast})

    Der Code wird statisch nach bekannten Schwachstellen geprüft. Dazu gibt es eine große Liste an
    Programmiersprachen und Frameworks, die unterstützt und automatisch erkannt werden.
- **Infrastructure as Code (IaC) Scanning** (Quelle: \cite{gitlab-sast-iac})

    Der Infrastruktur Code, z. B. für Docker-Container, wird auf Schwachstellen überprüft. Dies
    ähnelt dem SAST Test. In diesem Projekt wird kaum mit Docker-Containern gearbeitet, von daher
    hatte dieser Test kaum einen Effekt.
- **Dependency Scanning** (Quelle: \cite{gitlab-dependency-scanning})

    Überprüft die Abhängigkeiten der Projekte, ob für diese Schwachstellen bekannt sind. Auch hier
    werden unterschiedliche Programmiersprachen und Frameworks unterstützt.
- **Secret Detection** (Quelle: \cite{gitlab-secret-detection})

    Durchsucht den Quellcode, ob Passwörter, Schlüssel, Api-Tokens oder andere sensitive
    Informationen enthalten sind. Dies kann bei einem Entwickler durch Zufall passieren und
    gleichzeitig die Infrastruktur gefährden, wenn diese Informationen immer gültig sind.

Diese Tests liefen bei jedem Durchlauf der CI Pipeline mit und konnten somit schnell Probleme
aufzeigen. Das ist äußerst praktisch, da dies dem Entwickler dabei hilft, sicheren Quellcode zu
schreiben.

Einige dieser Tests benötigen eine bestimmte Lizenz von GitLab. Zum Start der Entwicklung hatte der
Provider der GitLab Instanz (GWDG) noch all seinen Nutzern eine Ultimate-Lizenz bereitgestellt,
wodurch alle Funktionen verfügbar waren. Im Laufe der Entwicklung wurde diese auf eine
Community-Lizenz umgestellt (siehe \cite{gwdg-umstellung-gitlab}), wodurch einige Tests (in diesem
Fall "Dependency Scanning") nicht mehr zur Verfügung standen. Dies hat natürlich auch Einfluss auf
die zukünftige Entwicklung, da Tests, die bisher genutzt wurden, von nun an nicht mehr
funktionieren.
