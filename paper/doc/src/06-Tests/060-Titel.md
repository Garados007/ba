\cleardoublepage

# Tests

Hier wird auf die Tests genauer eingegangen, die bei der Entwicklung der Anwendung durchgeführt
wurden. Aufgrund der Größe und des Umfangs des Projektes und gleichzeitig dem knappen Zeitrahmen
wurde auf viele Tests verzichtet. Vieles wurde manuell getestet und es wurden so gut wie keine
automatischen Tests vorgenommen. Dafür wurde bei der Entwicklung darauf geachtet, das alles so zu
entwickeln, dass ein Testen möglichst leicht ist und möglichst viele Fehler ausgeschlossen werden.

Es ist nicht auszuschließen, dass das derzeitige Produkt noch Fehler enthält. In der aktuellen
Version läuft dies aber zuverlässig auf sämtlichen Testgeräten ohne Abstürze oder Datenverlust.
