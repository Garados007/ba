## Grundsätze der IT-Sicherheit

Die IT-Sicherheit unterscheidet folgende Ziele: Vertraulichkeit (\ref{0310-vertraulichkeit}),
Integrität (\ref{0310-integrituxe4t}) und Verfügbarkeit (\ref{0310-verfuxfcgbarkeit}; siehe
\cite[6--11]{eckert-it-sicherheit2012}). Dazu kommen noch weitere Schutzziele wie Authentizität
(\ref{0310-authentizituxe4t}), Verbindlichkeit (\ref{0310-verbindlichkeit}), Zurechenbarkeit
(\ref{0310-zurechenbarkeit}) und Resilienz (\ref{0310-resilienz}) hinzu. Was diese Begriffe bedeuten
und wie diese im Vergleich zu der DSGVO und Richtlinien der Europäischen Kommission stehen, wird in
den folgenden Unterkapiteln beleuchtet.

### Vertraulichkeit

Die Daten selbst dürfen nur von autorisierten Nutzern gelesen und bearbeitet werden. Dies gilt auch
für den Zugriff auf gespeicherte Daten oder die Übertragung dieser.

Für eine Autorisierung stehen einem eine große Liste an Möglichkeiten zur Auswahl. Hier ein paar
Beispiele:

- Verwendung von Benutzername und Passwort
- Nutzung eines Auth-Tokens (z. B. ID-Karte mit Chip und/oder NFC, USB-Sticks)
- Biometrische Daten wie Gesichtserkennung oder Fingerabdrucksensor
- externe Anbieter über die Schnittstelle LDAP oder OAuth nutzen
- physische Liste mit Einmalpasswörtern (z. B. die TAN Liste, welche früher von Banken genutzt wurde)
- Anmeldecodes per SMS oder E-Mail (wird meist zur Verifizierung als 2. Faktor genutzt)
- Kurzlebige Codes über Apps externer Anbieter (z. B. Google Auth, Microsoft Authenticator)

Es wird empfohlen, mindestens zwei dieser Möglichkeiten zu verbinden (Zwei-Faktor-Authentifizierung
\cite{bsi-2fa}), um einen möglichst guten Schutz erhalten.

Den Zugriff auf die gespeicherten Daten lassen sich mit folgenden Möglichkeiten absichern:

- physisches Gerät mit Daten vor unbefugten Zugriff schützen: z. B. Laptop nicht stehen lassen,
    Gerät nicht weitergeben
- Datenspeicher vor Zugriff schützen: Dies lässt sich z. B. mit dem Rechtesystem des Betriebssystems
    erreichen.
- Daten vor Zugriff schützen: z. B. den kompletten Datenspeicher verschlüsseln und nur autorisierten
    Nutzern ermöglichen diesen Bereich zu entschlüsseln

Die sichere Übertragung der Daten geht vergleichsweise einfacher mit einer verschlüsselten
Verbindung, auch wenn es hier ein paar Hürden gibt. So soll auf ein etabliertes System (wie TLS)
gesetzt werden. Aber auch hier muss darauf geachtet werden, dass die verwendeten
Verschlüsselungsmethoden noch aktuell sind (z. B. MD5 und SHA-1 gelten mittlerweile als veraltet) und
die verwendeten Zertifikate noch gelten.

Zertifikate, solange diese von einer vertrauenswürdigen Stelle signiert sind, sind ein probates
Mittel, um den Schlüsselaustausch und die Authentizität der Gegenseite zu gewährleisten. Hier
empfiehlt es sich unter Umständen sogar Zertifikate in der Anwendung mitzuliefern und sich nicht auf
die installierten des Betriebssystems zu verlassen, da Nutzer (oder Viren) jederzeit unwissentlich
ein kompromittiertes Zertifikat installieren können.

### Integrität

> "Integrität bezeichnet die Sicherstellung der Korrektheit (Unversehrtheit) von Daten und der
> korrekten Funktionsweise von Systemen" \cite[34]{bsi-grundschutz2021}

Es gibt eine große Anzahl an Faktoren, welche die Integrität von Daten beeinträchtigen können.
Eine große Liste hat das BSI in seinem Grundschutzkompendium im Jahre 2021 aufgelistet (siehe
\cite[41--89]{bsi-grundschutz2021}).

Für dieses Projekt sind folgende Gefahrenquellen als besonders wichtig anerkannt wurden:

"Informationen oder Produkte aus unzuverlässiger Quelle" (\cite[62]{bsi-grundschutz2021}) können die
Integrität stark gefährden, indem Daten zum einen unvollständig durch den Forscher oder externe
Anwendungen aufgenommen werden. Dies kann z. B. passieren, wenn Bilder beim Upload abgebrochen oder
manipuliert werden oder fehlerhafte Imports vorgenommen werden. Gleichzeitig können aber auch
Drittprogramme die Schnittstellen der Anwendung fehlerhaft ansprechen.

Um hier den Schaden möglichst gering zu halten, wird allen Anfragen (egal ob vom Nutzer oder anderen
Anwendungen oder auch sich selbst) grundsätzlich nicht vertraut und geprüft. Das bedeutet zwar, dass
in der Regel Daten mehrfach geprüft werden, erhöht dafür aber die Wahrscheinlichkeit der
Korrektheit. Gleichzeitig wird an jeder Schnittstelle davon ausgegangen, dass Daten fehlerhaft oder
unberechtigt aufgenommen werden können und dafür gibt es dann entsprechende Fehlermeldungen. Falls
Daten nicht vollständig sind, so wird dies dem Nutzer auch mitgeteilt und nur vollständige
Datensätze werden bestätigt und weiterverarbeitet.

Ein weiteres Problem ist die "Manipulation von Hard- oder Software"
(\cite[63]{bsi-grundschutz2021}), bei der ein Nutzer oder Programme (Viren, Trojaner, ...) sich
einen Zugang zum Datenspeicher oder der Anwendung verschaffen und manipulieren.

Sämtliche gespeicherte Daten sind permanent verschlüsselt auf der Festplatte und lassen sich auch
ohne mehrstufige Anmeldung nicht entschlüsseln. Eine Manipulation der gespeicherten Daten kann aber
dazu führen, dass Anmeldungen und somit Entschlüsselung der Datenbank fehlschlagen, da die
benötigten Daten entfernt wurden. Auch die Manipulation der Datenbankdateien selbst kann im
schlimmsten Fall dazu führen, dass diese nicht mehr lesbar wird und daher die Daten verloren sind.
Gleiches gilt auch für die verschlüsselten Datenbank-Logs und Dateien. Gegen diesen
Integritätsverlust helfen nur Back-ups.

Bei einer "Fehlfunktion von Geräten oder Systemen" (\cite[68]{bsi-grundschutz2021}) kann z. B. das
komplette Gerät ausfallen oder unzuverlässig arbeiten. Dies kann durch verschiedene Faktoren wie
Alter, Unfälle (wie z. B. Wasserschaden, siehe \cite[45]{bsi-grundschutz2021}), fehlerhafte
Programmierung oder unsachgemäße Benutzung des Nutzers geschehen. Diese habten dann meist zur Folge,
dass die Daten fehlerhaft auf die Festplatte geschrieben werden, unwiderruflich beschädigt oder
verloren sind. Hier hilft nur eine gute Back-up-Strategie.

### Verfügbarkeit

Systemausfälle müssen verhindert werden und die Daten sollen nach einen vorher vereinbarten
Zeitrahmen wieder verfügbar sein. Systemausfälle lassen sich leider nicht immer vermeiden und wie
sorgfältig der Forscher mit dem Gerät umgeht, hat einen großen Einfluss darauf, da sich dagegen kaum
Vorbereitungen treffen lassen. Wofür sich Vorbereitungen treffen lassen, ist die Wiederherstellung
der Daten durch Back-ups. Indem regelmäßig Back-ups erstellt werden, ist es in einem angemessenen
Zeitrahmen wieder möglich, die Daten darüber wiederherstellen. Es besteht zwar immer noch das
Problem, dass beides gleichzeitig ausfallen kann, aber dafür wird die Wahrscheinlichkeit als extrem
gering angesehen.

Ein Problem bei der Wiederherstellung durch Back-ups ist, dass dies nur ein altes Abbild der Daten
darstellt. Sämtliche Daten, die danach generiert wurden, sind somit unwiederherstellbar verloren.
Hier ist Abhilfe nur darüber möglich, indem das Back-up-Zeitfenster relativ kurz gewählt wird, damit
der Umfang an verlorenen Daten nicht so groß ist.

Für die erste Iteration der Softwareentwicklung des Projektes soll behelfsmäßig zunächst ein
Cloud-Speicher zur zusätzlichen Speicherung der Daten verwendet werden. Dies ist keine
Back-up-Strategie per se und muss zusammen mit den Auftraggebern noch entwickelt werden.

Als Back-up-Zeitfenster wird maximal 1 Tag empfohlen. Das ist ein guter Kompromiss zwischen zu vielen
Back-ups (Cloud-Speicher wird schnell voll) und der Menge an Daten, die verloren gehen können. Im
schlimmsten Fall verliert der Forscher einen Tag seiner Arbeit.

### Authentizität

Die Daten müssen auf Echtheit und Vertrauenswürdigkeit geprüft werden können. Dies erfolgt in erster
Linie dadurch, dass sämtliche Daten verschlüsselt auf der Festplatte liegen. Sämtliche Schlüssel
lassen sich nur erhalten, indem sich der Nutzer in der Anwendung anmeldet und somit die Daten für
ihn zugänglich macht. Ist eine Anmeldung nicht möglich, so kann dies an ungültigen Anmeldedaten oder
der Authentizität der gespeicherten Daten liegen.

Eine weitere Stelle, wo die Authentizität geprüft wird, ist die Kommunikation zwischen der
Oberfläche und dem Hintergrundserver. Die meiste Kommunikation erfolgt über eine
WebSocket-Schnittstelle. Da dies einen festen Tunnel darstellt, wird hier die Authentizität beim
Aufbau der WebSocket-Verbindung geprüft. Auch hier gilt: Dem Nutzer wird nicht vertraut. Sämtliche
Anfragen werden geprüft, ob der Nutzer überhaupt befugt ist, die Anfragen zu machen. Die Anmeldung
erfolgt über den gleichen Tunnel und stellt somit sicher, dass alles zusammengehört und sich kein
Dritter einmischen kann.

Nicht jede Kommunikation zwischen Oberfläche und Hintergrundserver erfolgt über die
WebSocket-Verbindung. Für den Zugriff auf einzelne Dateien und Up- oder Downloads gibt es eine
REST-API. Hierfür gibt es kurzlebige Tokens, welche direkt mit einer WebSocket-Verbindung verknüpft
sind und sich auch nur über diese erhalten lassen. Ohne diese Tokens wird die Anfrage nicht vertraut
und die Anfrage wird nicht beantwortet.

Des Weiteren werden externe Anfragen von anderen Geräten in der Standardkonfiguration nicht
vertraut. Daher ist der Hintergrundserver so eingestellt, dass nur Anfragen vom gleichen Gerät
angenommen werden. Somit wird versucht, sicher zu stellen, dass der aktuelle Nutzer möglichst vor dem
Gerät sitzt. Dies garantiert einem zwar nicht, dass kein Proxy genutzt wird, dafür reduziert es aber
ein potenzielles Einfallstor für Angriffe.

### Verbindlichkeit

Ein unzulässiges Abstreiten durchgeführter Handlungen ist nicht möglich. Sämtliche Aktionen werden
durch den Nutzer induziert und werden auch nur von ihm akzeptiert. Sobald der Nutzer sich angemeldet
und eine Datenbank geöffnet hat, ist er somit berechtigt, diese auch zu bearbeiten. Jede Aktion wie
erstellen, bearbeiten oder löschen von Daten wird direkt durch den Nutzer ausgelöst. Die Anwendung
macht nichts, ohne den Befehl des Nutzers.

Für kritische Aktionen wie Löschen von Daten sind entweder die Menüs so strukturiert, dass diese
sehr übersichtlich sind, was gerade getan wird und der Nutzer dies bestätigen muss, oder es gibt
Wiederherstellungsfunktionen.

Es gibt aber auch Aktionen, die indirekt durch den Nutzer ausgelöst werden. Dazu zählt in erster
Linie die automatische Speicherung der Änderung von Einträgen. Dies verhindert Datenverlust und
sorgt für eine bequemere Nutzung.

Eine zweite Aktion wäre die automatische Erstellung und Aktualisierung des Suchindexes. Dies ist
notwendig, damit die Suche von Einträgen schnell vorangeht und der Nutzer nicht gezwungen ist, dies
selbst nach jedem Bearbeitungsschritt durchzuführen. Dies geschieht aber nur nach Neuanlegen von
neuen Einträgen oder der Speicherung von Bearbeitungen dieser.

### Zurechenbarkeit

Eine durchgeführte Handlung lässt sich den Verantwortlichen jederzeit zuordnen. Da die Anwendung nur
lokal auf dem Gerät des Forschers betrieben wird und auch jede Aktion auch nur von einem Nutzer
ausgehen darf, lässt sich diese Frage jederzeit beantworten: vom Nutzer selbst.

### Resilienz

Das System muss widerstandsfähig gegen Ausspähungen, irrtümliche oder mutwillige Störungen oder
absichtlichen Schädigungen (Sabotage) sein. Alle Aktionen geschehen nur auf dem Gerät des Forschers
und Störungen können daher nur vom Gerät selbst kommen. Von außen ist in der Standardkonfiguration
kein Eingreifen möglich.

Das Ausspähen von Daten ist relativ schwierig, da die Daten nur verschlüsselt auf der Festplatte
vorliegen. Doch können diese vom Bildschirm aufgezeichnet werden, wenn diese gerade entschlüsselt
dem Forscher im Browser angezeigt werden. Genauso lässt sich nicht ausschließen, dass mit
verschiedenen Proxys die Kommunikation zwischen Browser und Server ausgespäht wird. Dies ist auf der
anderen Seite recht schwierig durchzuführen, da Server und Oberfläche prüfen, ob gleiche Ports
genutzt werden.

Eine irrtümliche oder mutwillige Störung am gleichen Gerät lässt sich nicht ausschließen. So können
Prozesse einfach beendet, Werte im Arbeitsspeicher verändert oder ausgelesen oder Daten auf der
Festplatte verändert werden. Dies kann dazu führen, dass die Anwendung abstürzt und die Datenbanken
unbrauchbar werden. Nur mit Back-ups und Neustarts lässt sich dies wiederherstellen.

Außerdem ist die Anwendung nicht davor gewappnet, dass der Programmcode dekompiliert und verändert
wird. Mit einer veränderten Version der Anwendung können sämtliche Daten offengelegt oder neue
Schwachstellen eingebaut werden. Eine Änderung der Anwendung könnte mit einer Prüfung der Signatur
der Anwendung geprüft werden. Dies findet aktuell nicht statt, kann aber durch eine zukünftige
Erweiterung hinzugefügt werden.
