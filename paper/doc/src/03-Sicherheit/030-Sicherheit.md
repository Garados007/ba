\cleardoublepage

# Sicherheit

Dem Sicherheitsaspekt wurde einer hohe Bedeutung und Priorität zugewiesen. Dafür gibt es eine Liste
an Gesetzen, die eingehalten, oder Richtlinien, welche zu befolgen sind. Das Wichtigste daraus
ist die europäische Datenschutz-Grundverordnung DSGVO (im Englischen dir General Data Protection
Regulation GDPR).

Zu den angewandten Richtlinien zählt auch die der Europäischen Kommission
\cite{eu-regulation-ethics-dataprotection}, welche zudem auf die Sicherheit und den Schutz der
Forschungsdaten eingeht. 

Außerdem hat sich in der IT eine Liste an Grundsätzen durchgesetzt, auf welche in Kapitel
\ref{0310-grundsuxe4tze-der-it-sicherheit} genauer eingegangen wird.
