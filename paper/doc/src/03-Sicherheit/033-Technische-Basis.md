## Technische Basis

Aufgrund von den Grundsätzen der IT-Sicherheit (siehe \ref{0310-grundsuxe4tze-der-it-sicherheit}),
und dem Datenschutz (siehe \ref{0320-datenschutz}) lässt sich die technische Basis für die
Sicherheit herleiten.

Der Forscher arbeitet mit hoch sensitiven Daten (Patientenakten, religiöse Einstellungen,
persönliche Meinung, ...), welche den höchsten Schutz genießen und nicht in fremde Hände geraten
oder missbraucht werden dürfen. Dies hat eine höhere Priorität als der Verlust der Daten an sich.

Daher werden die komplette Datenbank und die gespeicherten Dateien durch die Anwendung
verschlüsselt und sind auch ohne den Schlüssel nicht entschlüsselbar. Es wird empfohlen, zusätzlich
die Festplatte zu verschlüsseln. Dies ändert nichts an der Verschlüsselung der Daten selbst, dafür
wurde aber eine zusätzliche Hürde vor dem Zugriff von außen eingeführt.

Des Weiteren muss sichergestellt werden, dass auch nur der Nutzer (in diesem Fall der Forscher)
Zugriff auf seine Daten hat und auch sonst niemand anderes. Dafür übergibt der Nutzer beim Start der
Sitzung den Schlüssel an die Anwendung, welcher direkt wieder weggeschmissen wird, sobald der Nutzer
die Sitzung beendet. Zusätzlich muss der Nutzer vor dem Start der Sitzung einen zweiten Faktor für
die Autorisierung bereitstellen. Hier wird ein Hardwaretoken verwendet. Das hat den Vorteil, dass
sich nicht auf externe Dienstleister oder Infrastruktur verlassen werden muss, da alles lokal am
Rechner geprüft wird.

Die Daten aus der Datenbank (und den gespeicherten Dateien) werden nur dann entschlüsselt, wenn dies
auch vom Nutzer gewünscht oder indirekt gefordert wird. Diese sind dann nur für den benötigten Zweck
im Arbeitsspeicher bereitgehalten und werden direkt im Anschluss wieder gelöscht.

Daten können mit einen Forschungskollegen nur dann freigegeben werden, wenn dies explizit vom
Forscher gewünscht wird. Ohne das ist das Einlesen der Daten nicht möglich. Insbesondere der
Hardwaretoken verhindert den Zugriff über die gleichen Zugangsdaten von unterschiedlichen Standorten
aus.

Damit der Zugriff zur Datenbank durch Verlust oder Beschädigung des Hardwaretokens oder das
Vergessen des Passworts nicht verloren geht, wird im Vorfeld ein Masterpasswort für die Datenbank
erzeugt. Dies allein reicht aus, um den Zugriff wiederherzustellen.
