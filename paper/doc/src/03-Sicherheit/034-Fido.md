## FIDO

\begin{myfigure}{FIDO2 USB Key von NitroKey}{fido2-key} \centering
\includegraphics[scale=0.25]{nitrokey.jpg}
\newline
\begin{footnotesize}
    Ein FIDO2 USB Key vom deutschen Hersteller NitroKey. Dieser hat einen Touchsensor verbaut, um
    die Nutzerpräsenz zu prüfen. \cite{fido2-key-nitrokey}
\end{footnotesize}
\end{myfigure}

Ein wichtiger Teil in der Sicherheitsarchitektur nimmt die 2-Faktor-Authentifizierung ein, damit
zusätzlich zum Wissen (Nutzername + Passwort) auch Besitz (Hardware) geprüft werden kann. Für dieses
Projekt wurde sich für einen FIDO2 kompatiblen USB-Stick entschieden. Das ist ein kleiner Chip,
welcher ein Kryptografie-Modul und einen Zertifikatsspeicher enthält. Dieser wird per USB an den
Computer (oder Handy) angeschlossen und tauscht mit diesem Daten aus.

Der Computer kann Anfragen an den USB Key senden, damit bestimmte Datensätze von diesen signiert
werden. Die privaten Schlüssel für die Signatur sind nur auf dem USB Key gespeichert und lassen sich
nicht von außen auslesen.

Alle gängigen Webbrowser (Mozilla Firefox, Google Chrome, Safari, Chromium und Opera) unterstützen
ein Protokoll namens WebAuthn, welches FIDO2 nutzt. Dies ist derzeit zwar noch im Entwurfsstadium
(\cite{webauthn-spec}), findet aber mittlerweile bei immer mehr Webseiten Einsatz (z. B. alle von
Google, Amazon, Microsoft, GitHub, GitLab; Quelle: \cite{webauthn-websites}).

Dabei können Webseiten über den Browser nach einen Public Key fragen und sich Daten signieren lassen
(für eine Anleitung siehe \cite{webauthn-guide}, für Demonstration siehe \cite{webauthn-demo}).
Darüber hinaus ist eine Registrierung und Anmeldung nur mit dem USB Key möglich, welche nicht
abgegriffen oder ausgespäht werden kann.
