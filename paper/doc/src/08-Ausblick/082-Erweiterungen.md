## Erweiterungen

Dies ist eine unvollständige Liste an Erweiterungen, die diese Anwendung in Zukunft erfahren kann.
In vorherigen Kapiteln wurden schon einige Erweiterungen angesprochen, hier gibt es weitere.

### Webseite

Im Rahmen dieser Arbeit ist eine Webseite leider nicht zustande gekommen. Eine Webseite wäre ein
guter Ort, um neuen Nutzern einen ersten Eindruck über das Produkt, seine Funktion und die Nutzung
zu vermitteln. Hier können auch Updates und Downloads bereitgestellt werden.

### Schnittstellen

Schnittstellen sind Möglichkeiten, um Daten aus anderen Programmen für diese Anwendung oder auch
anders herum bereitzustellen. Dazu können diese Daten importiert, exportiert oder auch direkt nativ
unterstützt werden, um ein nahtloses Arbeiten mit den Programmen zu ermöglichen.

Dabei gibt es verschiedene Formate zur Gestaltung dieser. Ein bekanntes, welches sich in der
ethnologischen Forschung etabliert hat, wären die Datenaustauschformate der DDI Alliance (siehe
\cite{ddi-products}). Diese hat verschiedene Schemas für XML, JSON und andere Dateiformate
bereitgestellt, damit sich hier Daten leichter von einer Anwendung in eine andere übertragen lassen.

Diese Formate zu unterstützen würde somit den Forscher erlauben, Daten aus beliebigen kompatiblen
Programmen in dieses und auch anders herum zu übertragen, um die Arbeit stark zu erleichtern, da die
Daten nicht mehrfach händisch neu angelegt werden müssen.

Es gibt aber weitere Formen von Daten, mit den ein Forscher eines ethnologischen Instituts häufig zu
tun hat. Dazu zählen auch die Programme Word und Excel der Office-Suite aus dem Hause Microsoft.
Hier können Erweiterungen bezüglich des Einlesens oder Generierung von Dokumenten entstehen. Auch
Plug-ins, damit direkt über die Programme Word oder Excel auf die Anwendung zugegriffen werden kann,
wären möglich.

### Cloud-Dienst

Derzeit ist die Anwendung nur lokal auf dem Laptop des Nutzers installiert und alle Daten sind auch
nur dort verfügbar und müssen auch von diesem gesichert werden. Dies kommt mit der Einschränkung,
dass die Daten nur umständlich geteilt und gesichert werden können. Eine Möglichkeit zu Erweiterung
wäre hier die Bereitstellung eines Servers, über die alles zentral gesichert und verwaltet wird.

Hier sind alle Daten zentral gespeichert und die Nutzer haben dann die Wahl, ob sie mit den
Live-Daten vom Server oder mit der lokalen Kopie (falls keine Internetverbindung besteht) arbeiten
möchten.

Besondere Vorteile an dieser Erweiterung sind:

- Daten lassen sich leichter durch die IT sichern und es lassen sich Back-up-Richtlinien stringend
  durchsetzen
- Das Teilen von Daten unter den Forschern untereinander ist leichter. Auch das gleichzeitige
  Arbeiten an gleichen Datensätzen wäre somit möglich.
- Der Forscher muss nicht mehr alle Daten lokal bereithalten. Damit würden auch die Anforderungen an
  das Endgerät gelockert werden. Eine Nutzung von Handys oder Tablets wäre somit möglich.
- Eine Installation ist nicht mehr zwingend notwendig, da die Weboberfläche auch zentral
  bereitgestellt werden kann. Dadurch wäre die Nutzung von Gastrechnern denkbar.
- Updates lassen sich leichter durchsetzen.

An dieser Erweiterung entstehen aber auch Probleme, die dann direkt berücksichtigt und behandelt
werden müssen:

- Die Daten müssen auf dem Server sicher vor externer Manipulation und Zugriff sein.
- Wie mit gleichzeitigen Änderungen umgegangen, die gegenseitig sich im Konflikt stehen?
- Wie lassen sich Rechte für die Datensätze vergeben? Wie granular geht das und wie wird das
  eingehalten? Das ist besonders für das Teilen der Datensätze notwendig.
- Wie werden nachträglich Daten synchronisiert, wenn der Nutzer wieder online ist?

### Suchoptimierungen

Die Suche lässt sich über verschiedene Wege noch weiter optimieren. Wichtig dabei ist, dass bei
allen Optimierungen darauf geachtet werden muss, dass sämtliche Daten weiterhin den gleichen
Sicherheitsstandard genießen, wie sie zuvor auch hatten. Ein paar Möglichkeiten, dies zu erreichen,
werden im Folgenden kurz ausgearbeitet.

Die erste Idee wäre, einen weiteren Suchindex für andere Datentypen aufzubauen. Derzeit existiert nur
einer für Strings, der auch Substrings ab der Länge 3 abdeckt. Daher wäre es hilfreich, wenn
jeweils für Ganzzahlen, Fließkommazahlen oder Datumsangaben ein weiterer Index existiert, der auch
Bereiche zwischen zwei Werte oder ähnliche Werte abdeckt. Die Werte sind zum Teil direkt durch die
Eingabefelder direkt zu ermitteln und zum Teil befinden die sich irgendwo in längeren Texten.

Ein weiterer Weg der Optimierung wäre, für besonders kurzen Strings einen Index aufzubauen, damit
auch nach Sachen wie "EU" gesucht werden kann.

Die dritte Idee wäre, eine passende Speicherstruktur für den Index zu entwerfen. Derzeit wird der
Index in einer Datenbank hinterlegt. Diese ermöglicht es zwar schnell einen bestimmten Schlüssel
abzurufen, muss aber alle Einträge abfragen, wenn der Nutzer alle Schlüssel in einem Bereich haben
möchte.

Weiterhin ist noch ein Index hilfreich, welcher auch historische Werte berücksichtigt. Mit diesen
können dann Forscher nach alten Werten suchen, bevor sie eine Änderung durchgeführt haben.

Des Weiteren weniger eine Geschwindigkeitsoptimierung, dafür mehr eine für die Nutzerfreundlichkeit
wäre ein einfacher Editor für die Suchanfragen. Somit muss ein Anfänger nicht erst verstehen und
lernen, wie Suchanfragen aufgebaut sind und was mit diesen alles möglich ist (siehe Anhang
\ref{9960-suchquery}).
