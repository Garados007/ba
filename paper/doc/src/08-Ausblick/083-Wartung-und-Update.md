## Wartung und Update

Geplant sind zukünftige Updates, welche Probleme und Fehler im aktuellen Produkt beheben, sowie
weitere Funktionen (siehe \ref{082-erweiterungen}) hinzufügen. Zur Verbreitung der Updates wird ein
ähnlicher Mechanismus wie bei der Installation (siehe \ref{071-installation}) verfolgt. Die Pakete
werden zusammengepackt und online zum Download bereitgestellt. Das Produkt überprüft selbstständig
im Hintergrund auf Updates, informiert den Nutzer und leitet ihn zum Download und Installation an.

Auch eine automatische Installation ist möglich, auch wenn dies unter Einschränkungen durch die Art
der Installation stehen kann. Dies ist zum Beispiel der Fall, wenn die Anwendung im
Programmverzeichnis installiert wurde und derzeit kein befugter Administrator verfügbar ist, der das
Passwort eingeben kann. Auch hierfür existieren Lösungen (z. B. über einen autorisierten
Updatedienst), welche aber speziell noch eingerichtet werden müssen.

In welcher Art und Weise die Wartung und Updates erfolgen, ist derzeit noch ein Bestandteil, der in
der Nachnutzung (siehe \ref{081-nachnutzung}) geklärt wird.
