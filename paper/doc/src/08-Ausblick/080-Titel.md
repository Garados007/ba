\cleardoublepage

# Ausblick

Nachdem das Projekt abgeschlossen und die Arbeit abgegeben ist, geht es darum, wie danach damit
verfahren wird. Wird es weiterhin im Einsatz sein (siehe Nachnutzung \ref{081-nachnutzung})? Gibt es
Erweiterungen und von welcher Art wäre vorstellbar (siehe \ref{082-erweiterungen})? Und wie wird mit
Updates und Wartungen verfahren (siehe \ref{083-wartung-und-update})?
