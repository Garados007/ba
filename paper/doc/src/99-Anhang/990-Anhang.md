\pagebreak
\appendix
\pagestyle{fancy2}

<!-- % \setcounter{section}{0} -->
\renewcommand\thesection{}
\renewcommand\thesubsection{\Alph{subsection}}
<!-- \renewcommand\thesection{A} -->

<!-- # Anhang -->

# Anhang {-}

Der Anhang sind weitere Dokumentationen zum Projekt und zur Arbeit enthalten. Quellcode ist nur auf
der beiliegenden CD verfügbar.

Die Dokumentationen sind teilweise auf Englisch, weil das Programm selbst auf Englisch dokumentiert
wurde. Es wurden nicht alle Dokumentationen auf Deutsch übersetzt.

Die Kapitel \ref{9940-rollenschema} und \ref{9950-dacryptero---internal-websocket-api} sind die
gerenderte Version des Schemas. Das originale Schema befindet sich als Json-Schema bzw.
AsyncApi-Schema auf der Beiliegenden CD. Diese Schemas wurden mit dem beiliegenden Tool
`/tools/AsyncApiRenderer` in die dargestellte Repräsentation umgewandelt.

Bei der AsyncApi wird eine Liste an Response-Nachrichten angezeigt. Dies sind Nachrichten, die der
Server als Antwort auf die Anfrage senden kann. Die Nachrichten können zeitlich wiederholt oder
mehrere Unterschiedliche hintereinander gesendet werden. Dies obliegt der Aufgabe, die der Server
mit der Anfrage bearbeitet.

\newpage

## Entity Relationship Diagramm v0.6

\includegraphics[scale=.28,angle=90]{data.v8.png}

Erklärung siehe Abbildung \ref{db-schema-er}.

