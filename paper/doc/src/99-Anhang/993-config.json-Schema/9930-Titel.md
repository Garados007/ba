## config.json Schema

Informationen und Erklärungen sind im Kapitel \ref{0444-schluxfcsselverwaltung}. Für den Vergleich
gibt es hier zwei Repräsentationen:

1. Das originale Schema, welches so aufgebaut ist, wie es nach
   [json-schema.org](https://json-schema.org) spezifiziert ist. Dies wurde manuell vom Autor
   erstellt.
2. Eine eigene Darstellung des oben gezeigten Schemas, welches das Schema als generierte Tabelle und
   generiertes Beispiel anzeigt. Der Generator befindet sich mit auf der CD als Quellcode unter
   `/tools/AsyncApiRenderer` und wurde für dieses Projekt speziell angefertigt.

Alle weiteren Darstellungen und Beispiel im Anhang nutzen die 2. Repräsentation. Die 1.
Repräsentation aller Anhänge befindet sich auf der CD im Ordner `/schemas`.

**1. Darstellung:**

```json
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "type": "object",
    "properties": {
        "name": {
            "type": "string",
            "description": "Der Name der Datenbank. Muss der gleiche wie
der Ordner sein.",
            "examples": [ "test-db" ]
        },
        "recovery-key-hash": {
            "type": "string",
            "description": "Der Base64 codierte SHA512 Hash des
Master-Keys.",
            "examples": [ "abdef01234567890..." ]
        },
        "role-schema": {
            "type": "string",
            "description": "Der Pfad zum Rollenschema innerhalb des
Schemaordners. Dieses Schema gilt für die gesamte Datenbank."
        },
        "access": {
            "type": "array",
            "description": "Eine Liste von Zugangsdaten, die Zugriff auf 
diese Datenbank haben",
            "items": {
                "type": "object",
                "description": "Die Einstellungen zu einen speziellen 
Zugriff",
                "properties": {
                    "user": {
                        "type": "string",
                        "description": "Der Nutzername des Nutzers, der 
Zugriff hat. Dieser darf mehrfach verwendet werden.",
                        "examples": [ "mustermann" ]
                    },
                    "user-id": {
                        "type": "string",
                        "format": "guid",
                        "description": "Eine GUID für diesen Nutzerzugang.
Wird für die Fido-Authentifizierung genutzt.",
                        "examples":
                            [ "d19482e9-e5df-4826-a824-235b5119dafc" ]
                    },
                    "password-salt": {
                        "type": "string",
                        "description": "Base64 codierte zufällige
Byte-Folge, die zusammen mit dem Passwort verarbeitet wird.",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "password-hash": {
                        "type": "string",
                        "description": "Base64 codierter Hash aus
Passwort und Salt",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "fido-descriptor": {
                        "type": [ "null", "object" ],
                        "description": "Gespeicherte Informationen zum
Fido USB Stick. Format ist durch die Bibliothek bestimmt.",
                        "examples": [
                            {
                                "Type": 0,
                                "Id": "ABDEFabcdef5678",
                                "Transports": null
                            }
                        ]
                    },
                    "fido-public-key": {
                        "type": [ "null", "string" ],
                        "description": "Base64 codierter Public Key vom
Fido USB Stick",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "fido-user-handle": {
                        "type": [ "null", "string" ],
                        "description": "Base64 codierter User Handle vom
Fido USB Stick",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "fido-counter": {
                        "type": [ "null", "integer" ],
                        "description": "Der Counter Wert bei der letzten
Anmeldung.",
                        "examples": [ 1024 ]
                    },
                    "enc-salt": {
                        "type": "string",
                        "description": "Base64 codierte zufällige
Bytefolge, die mit dem Passwort gehasht wird.",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "enc-key": {
                        "type": "string",
                        "description": "Base64 codierte, mit AES
verschlüsselte Datenbankschlüssel. Um diesen zu entschlüsseln ist der Hash
aus enc-salt und Passwort nötig.",
                        "examples": [ "abdef01234567890..." ]
                    },
                    "created": {
                        "type": "string",
                        "format": "date-time",
                        "description": "Der Zeitstempel an dem dieser
Zugriff angelegt wurde."
                    }
                },
                "required": [
                    "user",
                    "user-id",
                    "password-salt",
                    "password-hash",
                    "fido-descriptor",
                    "fido-public-key",
                    "fido-user-handle",
                    "fido-counter",
                    "enc-salt",
                    "enc-key",
                    "created"
                ]
            }
        },
        "contains-index": {
            "type": "boolean",
            "description": "Gibt an, ob ein Index für diese Datenbank
existiert. Wird nicht mehr verwendet.",
            "examples": [ "true" ]
        }
    },
    "required": [
        "name",
        "recovery-key-hash",
        "role-schema",
        "access",
        "contains-index"
    ]
}
```

**2. Darstellung:**
