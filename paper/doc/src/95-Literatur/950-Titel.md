\cleardoublepage

\pagestyle{fancy-empty}
\renewcommand\thesection{}

<!-- # Literatur und Quellen -->

\hypertarget{literatur-und-quellen}{%
\section*{Literatur und Quellen}\label{literatur-und-quellen}}
\addcontentsline{toc}{section}{Literatur und Quellen} 

\printbibliography[heading=none]
