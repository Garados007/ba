## Hürden und Herausforderungen

Auch bei diesem Projekt gab es bei der Umsetzung ein paar Stolpersteine, auf die in den folgenden
Unterkapiteln genauer eingegangen wird.

### WebAuthn

WebAuthn ist ein relativ neuer Standard. So neu, dass es zwar schon in jeden modernen Browser
unterstützt wird, es aber derzeit keine offizielle Spezifikation gibt (nur Entwürfe). Des Weiteren
ist diese jetzt auch nicht so verbreitet, dass der Entwickler sofort auch die nötige Technik parat
hat, um das zu testen.

So konnte der Autor sich am Anfang nur auf das eigene Mobilgerät verlassen, da der eigene
Fingerabdrucksensor als Hardwaretoken genutzt wird. Für die Umsetzung auf den Laptop wurden
FIDO-USB-Sticks bestellt, da zum einen nicht jeder Laptop einen Fingerabdrucksensor hat und zum
anderen der Sicherheitstoken als eigenständige physische Komponente genutzt werden soll.

Sobald die bestellten Hardwaretokens angekommen waren, ging es an die Implementierung der
Schnittstelle. Hier war das größte Problem, dass es nur wenige Beispiele und eine ausführliche, aber
leicht verwirrende Spezifikation online verfügbar ist. Ein weiteres Problem war die schier große
Menge an Funktionen und Variabilität an Schlüsseln, die WebAuthn bereitstellt. Nicht alles wird von
allen Tokens unterstützt und es ist schwierig herauszufinden, was benötigt wird und was nicht.
Schwierig wird es aber erst recht, wenn sämtliche Möglichkeiten der unterstützten
Schlüsselalgorithmen berücksichtigen werden sollen (z. B. ES256, EdDSA, HMAC in unterschiedlichen
Varianten, AES in unterschiedlichen Varianten, ...). Und dann gibt es eine Authentifizierung (siehe
\cite{webauthn-spec-register}), welche 24 Schritten beinhaltet.

Einen Großteil dieser Schwierigkeiten können verschiedene Bibliotheken (in diesem Projekt wurde
`fido2-net-lib` \cite{fido-net-lib} genutzt) übernehmen, welche aber alle wiederum eigene
Schnittstellen und Schwierigkeiten haben.

### Tests beim Nutzer

Es ist immer gut, schon frühzeitig Versionen den zukünftigen Nutzern zu zeigen, damit Feedback
schnell an den Entwickler gelangt. Dies wurde auch in dieser Arbeit versucht. Doch leider konnte
durch zeittechnische Schwierigkeiten seitens Nutzers und Autors nicht genauer auf Probleme
eingegangen werden.

