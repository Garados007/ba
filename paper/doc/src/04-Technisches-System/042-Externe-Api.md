## Externe Apis

Um den Zugang der Daten zu anderen Anwendungen zu ermöglichen, soll die Anwendung Schnittstellen
bereitstellen. Die einfachste Form hierzu ist der Export der gesicherten Dokumente, welche im
sicheren Speicher hinterlegt wurden. Nachdem diese exportiert (heruntergeladen) wurden, können diese
in einem anderen Programm betrachtet werden.

Genauso funktioniert der Prozess für den Import von Daten aus anderen Programmen in die Anwendung
hinein. Hier übernimmt die Anwendung nur die Aufgabe eines sicheren Speichers, in dessen die Daten
vor Manipulation von außen sicher sind.

Für die Daten in den Eingabefeldern gibt es bisher nur die Möglichkeit, die aktuelle Seite als PDF
über den Browser zu exportieren. Mehr Möglichkeiten sind hierzu als Erweiterungen (siehe
\ref{082-erweiterungen}) geplant.
