## Überblick

Die gesamte Anwendung ist in mehrere Module geteilt, welche für sich abgeschlossen und auch
austauschbar sind. Sie sprechen über eine einfache API miteinander und lassen sich separat
voneinander testen.

Hier wird ein lokaler HTTP Webserver genutzt, welcher nur Anfragen von localhost entgegennimmt,
verarbeitet und die Antworten zurückliefert. Der Nutzer kann dann eine beliebige Anwendung (in den
meisten Fällen ein moderner Webbrowser wie Firefox oder Chromium) nutzen und diesen Server
ansprechen. In den folgenden Fällen wird von diesem Modul als Back-End oder auch Server gesprochen.

Hinter dem Webserver wird eine lokale verschlüsselte Datenbank genutzt, bei der die komplette Verwaltung
im Prozess des Webservers eingebettet ist. Dazu wird die Open Source Bibliothek LiteDB genutzt, die
sich um die komplette Verwaltung dazu kümmert.

Des Weiteren gibt es das Front-End, welches aus einer Webseite besteht, welche von einem beliebigen
modernen Webbrowser dargestellt werden kann. Mit dieser Oberfläche wird der Nutzer hauptsächlich
kommunizieren und von den Vorgängen im Hintergrund sollte dieser eigentlich nichts mitbekommen. Im
Folgenden wird vom Front-End auch von der Oberfläche oder auch UI gesprochen.
