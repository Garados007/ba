### Interne APIs

All diese Module werden über verschiedenen Apis zusammengehalten und darüber wird auch kommuniziert.

Der Server und die UI kommunizieren hauptsächlich über eine einzelne WebSocket-Verbindung (siehe RFC
6455 \cite{rfc-6455}). Das hat den Vorteil, dass die Authentifizierung nur einmal am Anfang erledigt
werden muss und danach kann sich gegenseitig vertraut werden, solange die Verbindung nicht abbricht
(z. B. wenn der Nutzer die Seite im Browser neulädt). Außerdem können jederzeit Nachrichten vom
Server zur UI und auch anders herum sendet werden und so schneller auf neue Ereignisse reagieren.

Für Datei-Up- und Downloads wird zusätzlich eine kleine REST-API genutzt, damit zum einen hierfür
die Verbindungskapazität der WebSocket-Verbindung nicht ausgelastet wird und zum anderen die
Einbettung in die Oberfläche einfacher geschieht.

Der Server und die Datenbank kommunizieren über eine Open-Source-Bibliothek, welche im Prozess des
Servers angesiedelt ist. Über die API der Bibliothek wird dann die Datenbank verwaltet. Es findet
keine Inter-Process-Kommunikation statt - alle Daten sind sofort beim Server verfügbar und können
nicht mit einfachen Mitteln ausgespäht werden.
