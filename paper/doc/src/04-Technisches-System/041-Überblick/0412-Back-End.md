### Back-End (Server)

Der Server ist eine kleine C# Anwendung, welche sich um alles Wichtige im Hintergrund kümmert. Sie
nimmt alle Anfragen von der Oberfläche entgegen und informiert diese über Änderungen. Dann kümmert
dieser sich um die Verwaltung der Datenbanken und der verschlüsselten Dateien. Hier ist der
komplette Sicherheitsaspekt gelagert.
