\cleardoublepage

# Technisches System

Hier wird ein Überblick (siehe Abbildung \ref{technisches-system-ueberblick}) über das technische
System geliefert, dazu zählen die einzelnen Komponenten, die Apis, die Datenbank und wie ein
Anmeldeprozess aussieht.

\begin{myfigure}{Überblick über das technische System}{technisches-system-ueberblick} \centering
\includegraphics[scale=0.25]{überblick.png} \\
\begin{footnotesize}
    Diese Grafik zeigt einen Überblick über das technische System. Dazu sind alle Module und deren
    Schnittstellen gekennzeichnet. Die Module sind Back-End (siehe Kapitel
    \ref{0412-back-end-server}), Front-End (siehe Kapitel \ref{0411-front-end-ui}) und Datenbank
    (siehe Kapitel \ref{0440-datenbank}). Dazu wurden die internen API von WebSocket und REST in
    Kapitel \ref{0414-interne-apis} und den Anmeldeprozess über FIDO in Kapitel \ref{043-anmeldung}
    behandelt.
\end{footnotesize}
\end{myfigure}
