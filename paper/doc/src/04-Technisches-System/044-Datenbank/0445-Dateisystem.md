### Dateisystem

Die Anwendung speichert verschiedene Dateien im Dateisystem für die Datenbanken. Für die Datenbanken
gibt es einen vom Nutzer konfigurierten Haupt-Ordner. In diesen werden alle Datenbanken gespeichert.
Für jede Datenbank gibt es hierfür einen eigenen Unterordner. Der Namen des Unterordners darf nur
aus Buchstaben von `a` bis `z`, Ziffern, Bindestrich (`-`) oder Unterstrich (`_`) bestehen, zudem
dürfen sich zwei Datenbanken nicht den gleichen Ordner teilen und die Unterordner dürfen nicht
verschaltet sein.

In dem Datenbankordner gibt es dann eine Liste von Dateien und Ordnern, die von der Anwendung
genutzt werden:

- `config.json`: Die Konfiguration der Datenbank. Genauere Informationen dazu im Kapitel
  \ref{0444-schluxfcsselverwaltung}.
- `config.json.backup`: Wird vor der Aktualisierung von `config.json` von der Anwendung automatisch
  angelegt, um Fehler zu vermeiden.
- `confidential.db`: Die LiteDB Datei zur Confidential Datenbank
- `confidential-log.db`: Wird von LiteDB automatisch für `confidential.db` angelegt.
- `confidential.index.db`: Der LiteDB Index zur Confidential Datenbank
- `confidential.index-log.db`: Wird von LiteDB automatisch für `confidential.index.db` angelegt.
- `data.db`: Die LiteDB Datei zur Main Datenbank
- `data-log.db`: Wird von LiteDB automatisch für `data.db` angelegt.
- `data.index.db`: Der LiteDB Index zur Main Datenbank
- `data.index-log.db`: Wird von LiteDB automatisch für `data.index.db` angelegt.
- `files/`: Die gespeicherten verschlüsselten Dateien. Mehr siehe unten
- `upload/`: Die Dateien, die gerade vom Nutzer zum Server hochgeladen werden.

Zu den LiteDB-Dateien gibt es immer jeweils ein `-log.db` Datei. Diese wird automatisch vom System
angelegt und dient zur Wiederherstellung ungesicherter Daten. LiteDB sichert zuerst alle Änderungen
in diese Log-Datei und schreibt nach einer gewissen Zeit diese dann in die eigentliche Datenbank
rein. Dies geschieht alles automatisch, ohne dass der Entwickler eingreifen muss. Wenn Sicherungen
angelegt werden, so sind die Logdateien auch immer mitzusichern, da diese noch ungesicherte Daten
enthalten können.

Des Weiteren gibt es zur Main und Confidential Datenbank jeweils einen Index. Dieser kann jederzeit
vom Nutzer gelöscht werden. Die Anwendung legt diesen dann wieder neu an, indem die komplette
Datenbank neu indiziert wird. Dies können je nach Größe der Datenbank und Rechenleistung einige
Minuten bis eine Stunde dauern.

Wenn die Confidential Datenbank entfernt werden soll, so reicht es einfach aus den Dateien
`confidential.db` und `confidential-log.db` zu entfernen (Anwendung muss dabei neugestartet werden).
Die Anwendung kann dann problemlos damit weiterarbeiten. Ein einfaches Rücksichern ist möglich,
indem diese Dateien jederzeit wieder hinzugefügt werden.

Wenn Dateien vom Nutzer hochgeladen werden, dann landen diese zuerst im Ordner `upload/`. Dazu wird
ein zufälliger Schlüssel generiert und mittels AES in Echtzeit verschlüsselt. Die Daten sind also zu
jeder Zeit verschlüsselt auf der Festplatte, auch wenn die Übertragung nicht vollständig ist. Als
Dateiname wird Hex-Wert einer zufälligen 12 Byte großen Zahl gewählt. Fall der Upload abbricht, wird
einfach diese Datei gelöscht.

Sobald der Upload vollständig ist, wird die Datei in `files/` verschoben. Dafür wird zuerst der
SHA512 Hashwert der verschlüsselten Datei ermittelt. Als Pfad wird der Hex-Wert des SHA512
Hash-Werts genutzt, bei der die ersten beiden Bytes einen Unterordner und die nächsten beiden Bytes einen
Unterunterordner bilden. Zum Beispiel: `files/0D48/60D6/FA4F66CCB0C0D35C9FEAC8B...` (der Pfad wurde
gekürzt dargestellt). Die Wahrscheinlichkeit für eine Kollision ist hier äußerst gering. SHA512 kann
$1,3 \cdot 10^{154}$ verschiedene Zahlen abbilden (zum Vergleich, es gibt ca. $10^{80}$ Atome im
beobachtbaren Universum \cite{universe-atoms}). Und selbst für den äußerst unwahrscheinlichen Fall
gibt es ein Fallback: Es wird an den Dateinamen ein Bindestrich ("-") und eine dezimale Zahl
beginnend bei 1 angefügt, falls es zu einer Kollision kommt.

Nach dem Verschieben werden die Metainformationen in die Datenbank eingetragen und der Oberfläche
mitgeteilt.

Die Dateien in `files/` werden nicht entfernt oder exportiert, wenn die Confidential Datenbank
entfernt wird. Diese werden einfach von der Anwendung nicht mehr erkannt und können nicht gelesen
werden, da der Schlüssel fehlt. Für den Nutzer oder externe ist also der Inhalt der Dateien nicht
mehr erkenntlich. Einzig über die Dateigröße ließen sich Rückschlüsse ziehen. Dies wurde aber mit
dem Betreuer und dem Auftraggeber besprochen und dies soll nicht geändert werden.

Theoretisch ist es möglich, Dateien aus `files/` heraus zu löschen. Dies wird zwar nicht empfohlen,
aber die Anwendung erkennt dies und zeigt auch an, dass die Datei nicht mehr verfügbar ist. Es würde
nur für das Szenario, bei der der Nutzer alte Dateien aus der Historie entfernt haben möchte, Sinn
ergeben. Für diesen Fall ist derzeit kein Support in der Anwendung eingebaut.

