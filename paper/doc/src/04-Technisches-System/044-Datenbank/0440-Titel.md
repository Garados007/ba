## Datenbank

Die Datenbank umfasst verschiedene Arten von Daten. Darunter zählen die Einstellungen,
Zugangsberechtigungen, die Dateien, die eingegebenen Daten und die Metadaten des Forschers. Dadurch
ist diese auch relativ groß und komplex. In den folgenden Unterkapiteln wird genauer auf das DBMS
LiteDB, die Herleitung des Schemas, die Speicherung von Daten und die Arbeit mit diesen eingegangen.
