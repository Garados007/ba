### Schlüsselverwaltung

Die Schlüssel für die Entschlüsselung der Datenbankdatei befindet sich im Datenbankordner in der
`config.json` Datei. Hier sind alle Informationen enthalten, die benötigt werden, um Nutzer zu
autorisieren und authentifizieren. Die Datei nach dem Schema im Kapitel
\ref{9930-config.json-schema} (Anhang) aufgebaut.

Die Datei enthält einen Hash vom MasterKey, damit immer nachgeschaut werden kann, ob der aktuell
eingegebene MasterKey der Richtige sein könnte, bevor dieser direkt an der Datenbank angewandt wird.

Dann ist da eine Liste an Nutzerzugängen aufgelistet. Jeder Nutzer kann beliebig viele Zugänge
hinterlegt haben. Diese können sich in Benutzername, Passwort oder Fido gleichen oder unterscheiden.
Das macht für die Anwendung keinen Unterschied.

Zu jedem Passwort wird der gesalzene Hash hinterlegt. Gesalzene Hashes werden generiert, indem zu
dem Passwort ein zufällig generierter Hash angehangen wird und dann zusammen gehasht wird. In dieser
Anwendung wird der Prozess 10.000-mal wiederholt. Der Prozess des gesalzenen Hashens hat den
Vorteil, dass es deutlich erschwert wird, mithilfe von den Hashes die Passwörter zu knacken (siehe
\cite{salted-hashes}).

Zusätzlich dazu werden Informationen zum genutzten Fido-Key hinterlegt. Das Format dieser Werte ist
durch die verwendete Fido-Bibliothek vorbestimmt und wird durch die Anwendung nur gespeichert und
weitergereicht. Diese Daten werden genutzt, um den FIDO USB-Stick zu authentifizieren und zu
validieren.

Zum Schluss gibt es noch das verschlüsselte Datenbankpasswort, welches durch das Nutzerpasswort und
einem Salt entschlüsselt werden kann. Das entschlüsselte Datenbankpasswort entspricht dem MasterKey,
wird aber im späteren Verlauf durch die Anwendung nicht mehr bereitgestellt oder angezeigt.

Es ist mithilfe des Datenbankpassworts möglich, neue Zugänge in die JSON Datei einzutragen.
Existierende Einträge können jederzeit durch den Nutzer entfernt werden. Es ist technisch nicht
nötig, das Passwort hierzu zu wissen, da die JSON Datei einfach bearbeitet werden kann. Für die
Anwendung wäre es dann so, als ob es die Zugangsdaten nie gegeben hätte.

Wenn die Datei vom Nutzer in anderer Art verändert wird (Hinzufügen von nicht validen Einträgen oder
Löschen von validen), dann ist es möglich, dass der Zugang zur Datenbank verloren geht. Die Anwendung
kann damit umgehen, wenn an einer beliebigen Stelle die Authentifizierung fehlschlägt und meldet
dies auch dem Nutzer.

Falls die Datei verloren gegangen ist, dann lässt sich der Zugang nur über den MasterKey
wiederherstellen, da dieser das eigentliche Datenbankpasswort ist.


