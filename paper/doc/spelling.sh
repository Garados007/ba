#!/bin/bash

# Check for bad word or spelling usage

function check () {
    output=$(pcregrep -Mnri "$1" src)
    if [ "$?" = "0" ]; then
        echo -e "$(tput smso)\033[32m### $1 ###\033[39m$(tput rmso)"
        echo "$output" | sed -E "/^([^:]+:[0-9]+:)/s//`printf "\\033[31m"`\1`printf "\\033[39m"` /"
    fi
}

# check for todos
check '\Wtodo\W'
# check for Umgangssprache
check '\Wman\W'
check 'von\s+daher'
check '\Wwo\W'
# check for ungenaue Angaben
check 'größere'
