# Installation Guide

You have to fulfill the requirements and the setup to get a complete installation of the current
version of Dacryptero.

## Requirements

The requirements differ between the different operation systems a bit. Just look in the
corresponding sections.

These requirements are for the release version of Dacryptero. If you want to build your own version
you have to look in the developer manual.

### Windows

1. **Windows 10** or newer is required.
2. **.NET Runtime 6.0** or newer (Downloads
   [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)) required
3. **Firefox 97.0** or newer, or **Chromium 98** (this includes Google Chrome) or newer
4. at least **50 MB** free storage for the application and **2 GB** for the database
5. at least **2 GB** free RAM

### Linux

1. Your system should be up to date with the newest releases. How to do this look at the
   introduction for your distro.
2. **.NET Runtime 6.0** or newer (Downloads
   [here](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)) required
3. **Firefox 97.0** or newer, or **Chromium 98** (this includes Google Chrome) or newer
4. at least **50 MB** free storage for the application and **2 GB** for the database
5. at least **2 GB** free RAM

### Mac OSX

> **Untested!**

The support for this operating system is not tested. For the specification try to orientate at the
Linux spec-sheet.

## Installation

> This topic is in subject to change in the future.

### Windows

1. Create a folder in `C:\Programs\Dacryptero`
2. Copy the Dacryptero files in it.
3. Setup the configuration
4. Create the shortcut for the executable `C:\Programs\Dacryptero\Dacryptero.exe` and the browser
    `http://localhost:8015/`.

### Linux

1. Create the program folder

    ```bash
    sudo mkdir /usr/lib/Dacryptero
    ```
2. Copy the Dacryptero files in it.
3. Change the permissions.

    ```bash
    cd /usr/lib/Dacryptero
    sudo chown -R root:root .
    sudo chmod -R +r .
    sudo chmod +x Dacrytero
    ```
4. Create the link to include executable in the path

    ```bash
    sudo ln -s /usr/lib/Dacryptero/Dacryptero /usr/bin/Dacryptero
    ```

### Mac OSX

The installation steps are unknown.
