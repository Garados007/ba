# Programmiersprachen

| Sprache | C# | Java | C/C++ | Rust | JavaScript | Elm | TypeScript |
|-|-|-|-|-|-|-|-|
| Erster Release | 2001 | 1995 | 1985 | 2015 | 1995 | 2012 | 2012 |
| Designer | Anders Hejlsberg | James Gosling, Sun Microsystems | Bjarne Stroustrup | Graydon Hoare | Brendan Eich | Evan Czaplicki | Microsoft |
| Entwickler | Microsoft | Sun Microsystems, Oracle | Bjarne Stroustrup | Mozilla, Graydon Hoare, Rust Fundation | Brendan Eich | Evan Czaplicki | Anders Hejlsberg, Microsoft |
| Zielsprache | IL | Java Byte Code | Maschinencode | Maschinencode | - | JavaScript | JavaScript |
||
| **Paradigmen** (lt. Wikipedia) |
| strukturiert        | x |   | x | x |   |   | x |
| imperativ           | x |   | x | x |   |   | x |
| deklarativ          | x |   |   |   |   |   |   |
| objektorientiert    | x | x | x |   | x |   | x |
| ereignisorientiert  | x |   |   |   |   |   |   |
| funktional          | x |   | x | x | x | x | x |
| generisch           | x |   | x | x |   |   |   |
| reflexiv            | x |   |   |   |   |   |   |
| parallel            | x |   |   | x |   |   |   |
| prozedural          |   |   | x |   | x |   |   |
| dynamisch typisiert |   |   |   |   | x |   |   |
| prototypisch        |   |   |   |   | x |   | x |
||
| **Zielsystem** |
| Server (Docker) | x | x | x | x | node | node | node |
| Browser | WASM | | WASM | WASM | x | x | x |
||
| **Verbreitung** |
| [PYPL-Index Oktober 2021](https://de.statista.com/statistik/daten/studie/678732/umfrage/beliebteste-programmiersprachen-weltweit-laut-pypl-index/) | 7,3% | 17,18% | 6,48% | 1,08% | 8,81% | | 1,91% |
| GitHub Repos (Stand: 19.10.2021) | 1 010 441 | 3 830 935 | 1 823 839 | 47 611 | 4 464 883 | 9 564 | 353 697 |

PYPL
: Der Index wie oft eine Tutorial zu der Programmiersprache auf Google gesucht wurde.

<!--
||
| **Eigenschaften** |
| Typsicherheit | ja | ja, schwach bei Generics | schwach | stark | keine | stark | ja |
| `null`-Probleme | x | x | x | | x | | ? |
| Geschwindigkeit (Server) | ok | langsam | schnell | schnell | sehr langsam | sehr langsam | sehr langsam |
| sonstige | | | | Speicher- und Typsicher mit der Geschwindigkeit von C/C++ | | Keine Laufzeitfehler | |
-->
