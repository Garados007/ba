# Verschlüsselung der Datenbank

Jede Datenbankdatei `D` ist mit einen eigenen Schlüssel `K` verschlüsselt (Main und Confidential
Database teilen den gleichen Schlüssel).

Jeder Nutzer hat ein eigenes Passwort `P` (dies besteht aus zwei Teilen, dem eigentlich Passwort und
der 2FA Kennung). `K` wird nun mit `P` verschlüsselt und in einer Tabelle hinterlegt. Die Tabelle kann
man sich in etwa so vorstellen:

| Nutzer | Datenbank | `hash(P)` | `encrypt(P, K)` |
|-|-|-|-|
| Alice | `D` | `abcd` | `0123` |
| Alice | `D2` | `abcd` | `9876` |
| Bob | `D` | `fedc` | `1a2b` |

Somit ist es möglich, dass Alice und Bob auf die gleiche Datenbank `D` zugreifen können, ohne das
Passwort des jeweils anderen zu kennen. Außerdem erlaubt diese Herangehensweise, dass Alice ihr
Passwort ändern darf, ohne die Datenbank komplett neu verschlüsseln zu müssen.

Ein Problem stellt sich nun auf, wenn aber Alice ihr Passwort vergisst und auch niemand anderes
sonst einen Zugang erhalten hat, dann wäre theoretisch die Datenbank für immer verschlossen. Dafür
wird K ein weiteres Mal verschlüsselt. Dafür wird ein neuer Schlüssel `S` erstellt und `K`
verschlüsselt. `encrypt(S, K)` wird abgespeichert und Alice bekommt, als die Datenbank zum ersten
Mal erstellt hat den Schlüssel `S` angezeigt, den sie nun sicher verwahren muss.

Sobald Alice keinen Zugriff mehr hat, wird sie nach der Eingabe von `S` gefragt. Sie geht zu ihren
sicheren Speicher und schaut dort nach (dies kann auch eine andere Person sein, welche den Schlüssel
ihr erst einmal zusenden muss). Danach kann sie ein neues Passwort `P2` setzen und in der Tabelle
werden die neuen Daten gespeichert, so dass es in etwa so aussieht:

| Nutzer | Datenbank | `hash(P)` | `encrypt(P, K)` |
|-|-|-|-|
| Alice | `D` | `4567` | `4829` |
| Alice | `D2` | `abcd` | `9876` |
| Bob | `D` | `fedc` | `1a2b` |
