# Rollenschema

Das Rollenschema wird als JSON Dokument gespeichert und sieht ungefähr so aus:

```json
{
    "name": "https://mydomain.com/schemas/test.json",
    "online": "https://mydomain.com/schemas/test.json",
    "roles": [
        {
            "name": "Person",
            "abstract": true,
            "parent": null,
            "attributes": {}
        },
        {
            "name": "Specialist",
            "abstract": false,
            "parent": "Person",
            "attributes": {
                "occupation": {
                    "type": "string",
                    "protected": false,
                    "history": true
                }
            }
        }
    ]
}
```

Dies entspricht dem ER-Datenmodell von Version 5. Eine wichtige Validierungsregel: Das Schema wird
von oben nach unten eingelesen. Von daher **muss** ein Parent definiert sein, bevor ein Kind dieses
verwenden möchte. Ein Verweis auf sich selbst ist nicht erlaubt.

Es ist nicht erlaubt, dass Kindstypen die gleichen Attribute wie ein Elterntyp definiert.
