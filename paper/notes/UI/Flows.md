# Flows

## Login Flow

1. Der Nutzer öffnet die Seite zum integrierten Server
2. Es wird eine einfache Nutzername/Passwortabfrage gestellt
3. Nach Eingabe wird dies geprüft. Bei Fehler wieder Schritt 2
4. Es wird der 2FA Schritt erwartet. Es wird nur ein Bild angezeigt.
5. UI erhält Erfolgsmeldung.
6. UI wechselt zum nächsten Bildschirm, Server bereitet Daten vor.

## Account erstellen

1. Nutzer öffnet Seite zum integrierten Server
2. Gibt an, dass neuer Nutzer angelegt werden soll.
3. Gibt Nutzername/Passwort ein. Nutzername kann wiederverwendet werden!
4. 2FA Token wird erwarten. Daten werden im Hintergrund vom Server verarbeitet. UI wartet.
5. Server meldet Erfolg
6. UI wechselt zum nächsten Bildschirm, Server bereitet Daten vor.

## Hauptinterface

1. UI wechselt Panel und fragt Server Daten ab
    - Daten aus vorherigen Panel werden verworfen.
    - Ref zum aktuellen Panel werden in die History gepackt.
2. Server sendet Daten
3. UI stellt dar.
