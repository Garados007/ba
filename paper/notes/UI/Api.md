# API

Für fasst alle Abfragen wird eine einfache Websocket API zur Oberfläche bereitgestellt. Dies hat den
Vorteil, dass die Oberfläche Anfragen stellen kann und nicht auf die Antwort warten muss. Sobald
die Antwort vom Server vorhanden ist, kann diese dargestellt werden. Außerdem lassen sich hierüber
auch Prozesse leichter darstellen oder Zwischenergebnisse streamen.

Für einen kleinen Teil wird dennoch auf eine REST API zurückgegriffen, da es sonst Probleme mit der
Limitierung der WebSocket API gibt. Darunter fallen folgende Aufgaben:

- Upload von Dateien zum Server
- Download von Dateien vom Server
- Bereitstellung von Resourcendateien (HTML, CSS, Bilder, ...)

## WebSocket

Die WebSocketverbindung bleibt über die Dauer der Nutzung bestehen und wird nicht geschlossen. Der
Server merkt sich die relevanten Daten über die Dauer der Existenz der Verbindung. Sobald die
Verbindung geschlossen wird, muss in einer neuen Verbindung ein neuer Authorisierungsprozess
stattfinden.

Bei Start der Verbindung dürfen nur Pakete zur Authorisierung gesendet werden. Sobald der Server
eine Verbindung mit der Datenbank bestätigt hat, können auch Anfragen zu allen verbundenen
Datenbanken gesendet werden. In bestimmten Situationen kann eine erneute Authorisierung erforderlich
sein (z.B. Anschluss neuer Datenbank, Erstellen von neuen Konten oder Freigabe von Medien). Dann
wird die UI wieder in den Authorisierungsmodus gesetzt, darf aber weiterhin Anfragen zu bestehenden
Datenbankverbindungen senden oder empfangen.

Der Server versucht von sich aus die Menge an Informationen in Paketen so zu wählen, dass die Pakete
nicht 1 MB überschreiten. Bei sehr langen Texten ist dies nicht immer möglich. Außerdem versucht der
Server bei langanhaltenden Prozessen oder Streams (Ergebnisse von Suchanfragen) die Ausgabe so zu
takten, dass es sekündliche Updates gibt. Bei Streams kann dies nicht immer funktionieren, wenn zum
Beispiel die Anfrage so komplex ist, dass die Auswertung eines einzelnen Elements schon länger als
eine Sekunde dauert.
