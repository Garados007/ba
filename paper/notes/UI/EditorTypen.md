# Datentypen für den Editor

Die Werte der Attribute können verschiedene Typen annehmen. Je nach Typ wird dann der passende Editor ausgesucht und eingestellt.

Folgende Editoren werden bereitgestellt:

| Editor | Fullscreen | Bemerkung |
|-|-|-|
| `DateTime` | x | Auswahl von Datum **und** Zeit. Angezeigt wird alles in der lokalen Zeitzone des Anwenders. |
| `SingleLine` | | Eingabe von einen einzeiligen kurzen Text. |
| `MultiLine` | x | Eingabe von mehrzeiligen unformatierten Text. |
| `Number` | | Eingabe von einer beliebigen Fließkommazahl. |
| `Int` | | Eingabe von einer ganzzahligen Zahl. |
| `Bool` | | Eingabe von An/Aus, Wahr/Falsch, Ja/Nein |

Wenn der Editor nicht unterstützt wird, so wird seine JSON Repräsentation als Read-Only angezeigt.

Wenn die JSON Repräsentation nicht zum Editor passt, so wird der Wert verworfen und ein Standardwert genutzt.
