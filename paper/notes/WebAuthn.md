# Web Authentication

Wie das funktioniert, zumindest wie ich es verstanden habe. Tests stehen noch aus:

Links:

- https://webauthn.guide/
- https://github.com/passwordless-lib/fido2-net-lib

## Account erstellen

1. Server erzeugt Nutzer-ID
2. Nutzer gibt seinen Nutzernamen ein
3. Browser fragt nach Token (JS)
4. Es werden Daten erstellt
5. Daten werden an Server übermittelt
6. Prüfung und Speicherung

Ergebnis:

- Nutzer ID + Name
- Public Key

## Anmeldung

1. Server fragt an einen Datensatz zu signieren
2. Browser fragt Nutzer nach Token (JS)
3. Token signiert den Datensatz
4. Signatur wird an Server übermittelt
5. Vergleich

Ergebnis:

- Nutzer ID
- seine Existenz

## Mögliches Szenario

1. Nutzer meldet sich mit Benutzername + Passwort an
2. `hash(Passwort)` werden für die Daten genutzt.
3. Token signiert die Daten
4. Signatur wird nach der Prüfung gehasht und als Schlüssel genutzt.
