# Implementierungsstatus der WebSocket Events

## Empfangene Events

| Event | Handled |
|-|-|
| DataInterviewAddRequest        | x |
| DataInterviewGet               | x |
| DataInterviewListPersonRequest | x |
| DataPersonAddRequest           | x |
| DataPersonGet                  | x |
| EditInterviewSend              | x |
| EditPersonSend                 |(x)|
| FileFetch                      | x |
| FilePersonFetch                | x |
| FileRename                     | x |
| InfoRequest                    | x |
| SchemaRequest                  | x |
| SearchCancel                   |   |
| SearchSend                     | x |

## Gesendete Events

| Event | Handled |
|-|-|
| DataInterviewAddSend        | x |
| DataInterviewListPersonSend | x |
| DataInterviewSend           | x |
| DataPersonAddSend           | x |
| DataPersonSend              | x |
| EditAccept                  | x |
| EditDenied                  | x |
| FilePersonSend              | x |
| FileSend                    | x |
| FileUpdated                 | x |
| InfoSend                    | x |
| SchemaResponse              | x |
| SearchResponse              | x |

InfoSend: UploadToken muss noch ersetzt werden.