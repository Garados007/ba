> Diese Arbeit ist in dem Zustand archiviert, wie diese am Prüfungsamt abgegeben wurde. Einzig
> diese Markierung und der Upload von zwei Dateien wurde vorgenommen. Die Weiterentwicklung findet
> [hier](https://gitlab.gwdg.de/mpieth/dienste/dacryptero/dacrypero-project) statt.
>
> Weitere Dokumente:
> - [Bachelorarbeit](Bachelorarbeit.pdf)
> - [CD Image](Bachelorarbeit-CD.zip)

# Planung und Entwicklung einer Datenbankanwendung für das Forschungsdatenmanagement

> Diese Arbeit ist als Bachelorarbeit entstanden. Für die Arbeit selbst siehe beiliegenden Dokument,
> in der Pipeline oder im `/paper/doc/` Ordner.

## Aufteilung der Ordner

Die Ordner enthalten **viele** Dokumente aus der Entwicklung, welche aktiv nicht mehr benötigt
werden. Diese werden nicht aufgelistet.

- `/paper/doc/`: Die Bachelorarbeit
- `/server/Dacryptero/`: Der Webserver
- `/tools/`: Alle Konfigurationen für automatische Builds in der CI Pipeline. Dies ist der beste
  Anlaufpunkt für genaue Build-Anweisungen.
- `/ui/`: Die Oberfläche
- `/.gitlab-ci.yml`: Hauptkonfiguration für die CI Pipeline

## Build: Bachelorarbeit

> CI: 
>
> 1. `/tools/build-asyncapi.gitlab-ci.yml`: `build_asyncapi`
> 2. `tools/build-paper.gitlab-ci.yml`: `build_paper`

1. Installation von Dotnet SDK 6.0, YQ, TexLive, Pandoc, Biber, Rsync
2. Folgende Schritte müssen nacheinander ausgeführt werden:
    ```bash
    yq -o json paper/notes/websocket-api.yml \
        > paper/notes/websocket-api.json
    [ ! -d paper/doc/src/99-Anhang/995-AsyncApi ] && \
        mkdir -p paper/doc/src/99-Anhang/995-AsyncApi
    cd tools/AsyncApiRenderer
    dotnet run -- \
        ../../paper/notes/websocket-api.json asyncapi '9950-' \
        > ../../paper/doc/src/99-Anhang/995-AsyncApi/9950-Titel.gen.md
    dotnet run -- \
        ../../paper/doc/src/99-Anhang/993-config.json-Schema/schema.json \
        json-schema \
        > ../../paper/doc/src/99-Anhang/993-config.json-Schema/9931-Tabelle.gen.md
    dotnet run -- \
        ../../paper/doc/src/99-Anhang/994-Rollenschema/schema.json json-schema \
        > ../../paper/doc/src/99-Anhang/994-Rollenschema/9941-Tabelle.gen.md

    cd ../..
    [ ! -d release/doc ] && mkdir -p release/doc
    cd paper/doc && ./build.sh
    ```

Das Ergebnis findet sich anschließend in `/paper/doc/bin/main.pdf`.

## Build: Anwendung

> CI:
>
> 1. `/tools/build-server.gitlab-ci.yml`: `build_mime-cache`
> 2. `/tools/build-server.gitlab-ci.yml`: `build_server`
> 3. `/tools/build-ui.gitlab-ci.yml`: `build_ui-elm`
> 4. `/tools/build-ui.gitlab-ci.yml`: `build_ui-compress-js`
> 5. `/tools/deploy.gitlab-ci.yml`: `deploy_bundle`

Bitte in die oben aufgelisteten CI Pipelines schauen. Die Schritte sind komplex, voneinander
abhängig und nehmen viel Platz ein. Das Ergebnis befindet sich anschließend in `/release`.

## Nutzung

Die Nutzung ist als Kurzanleitung für das Programm auf der CD gedacht. Es wird davon ausgegangen,
dass das Programm entsprechend der beiliegenden `HowTo.pdf` auf den Computer kopiert wurde. Es ist
**nicht** auf der CD lauffähig! Außerdem wird davon ausgegangen, dass die Anwendung über eines der
beiden `start` Skripten gestartet und die Seite [http://localhost:8015/](http://localhost:8015/) im
Browser geöffnet wurde.

Es wird nur kurz aufgezählt, was auf den jeweiligen Seiten möglich ist.

### Navigationsleiste

- `Home` öffnet die Seite Home
- `Search` öffnet die Seite Search mit einer leeren Anfrage
- `All Interviews` öffnet die Seite Search mit der Anfrage `type:interview sort:none` und startet
  diese sofort.
- `All Persons` öffnet die Seite Search mit der Anfrage `type:person sort:none` und startet diese
  sofort.
- darunter ist eine Jump-List der zuletzt geöffneten Seiten. Ein Klick auf einen Eintrag öffnet die
  Seite sofort und bringt diese an den Anfang der Liste.
- Die Browsernavigation (vorherige Seite, nächste Seite) funktioniert auch

### Seite: Home

- `Create Database` legt eine neue Datenbank an
- `Import Database` ist nicht implementiert
- `Connect Database` versucht eine noch nicht ensperrte Datenbank zu öffnen. Geht nur, falls es so
  eine gibt.
- Rechtsklick auf bestehende Datenbank öffnet ein Menü. Gleiches geht auf den drei Punkten auf der
  rechten Seite.
    - `Add Interview` fügt ein neues Interview zur Datenbank hinzu.
    - `Share` ist nicht implementiert
    - `Manage Access` verwaltet die Zugriffe
    - `Remove Confidential Database` ist nicht implementiert
    - `Re-Index Database` indiziert die komplette Datenbank neu
    - `Backup` ist nicht implementiert
    - `Remove` ist nicht implementiert
- `Add Schema` ist nicht implementiert
- Rechtsklick auf bestehendes Schema öffnet ein Menü. Gleiches geht auf den drei Punkten auf der
  rechten Seite.
    - `Inspect` ist nicht implementiert
    - `Download` ist nicht implementiert
    - `Remove` ist nicht implementiert

### Modal allgemein

- Über die Titelleiste ist ein Modal verschiebbar
- Ein Klick in ein Modal bringt dieses in den Vordergrund
- Das Augensymbol oben rechts blendet ein Modal aus. Dadurch wird im unteren Bereich eine Taskleiste
  angezeigt mit den Modal in der Liste. Darüber kann es wieder dargestellt oder geschlossen werden.
- Das Lupensymbol bringt das Modal in den Vollbild und überdeckt alle anderen Anzeigen.
- Das Kreuzsymbol oben rechts schließt das aktuelle Modal

### Modal: Add Database

Schritt 1:

- `Database Name` ist der Name der neuen Datenbank
- `Schema` ist das neue Schema
- `Create Database` geht zu Schritt 2

Schritt 2:

- `Show Master Key` zeigt den Master Key im neuen Browsertab an. Bitte ausdrucken und sicher
  hinterlegen.
- `I have saved the Master Key securely` erst bestätigen nachdem Master Key gesichert wurde
- `Add new User` öffnet ein Registrierungsdialog und fügt einen neuen Zugang hinzu.
- `Create` schließt Erstellung ab und schließt das Modal. Erstellung ist fertig und direkt
  verbunden.

### Modal: Manage Access

- Klick auf den Mülleimer und Bestätigung löscht den Zugang
- `Unlock using Login` öffnet die Anmeldung. Dadurch wird die Datenbank freigeschaltet
- `Unlock using Master Key` öffnet die Abfrage des Master Keys. Dadurch wird die Datenbank
  freigeschaltet.
- `Register new Credential` öffnet ein Registrierungsdialog. Dadurch wird einer neuer Zugang
  angelegt. Diese Option wird nur bei freigeschalteter Datenbank angezeigt.

Ist die Datenbank freigeschaltet reicht es aus das Modal zu schließen, damit diese wieder gesperrt
wird.

### Seite: Search

- Eingabe in der Suchleiste für die Suchanfrage. Dokumentation dazu im Anhang.
- Drücken der Entertaste oder ein Klick auf die Lupe startet Suche
- Auswahl der Ergebnisse öffnet den Eintrag
- Falls viele Ergebnisse verfügbar sind wird unten eine Seitenauswahl angezeigt.

### Seite: Interview

- Oben rechts auf dem Mülleimer kann man das Interview löschen
- Felder können beliebig ausgefüllt werden
- Bei People können bestehende Personen geöffnet werden
- Ein Klick auf das Plus bei People öffnet das `Add Person to Interview` Modal

### Modal: Add Person to Interview

- auf der linken Seite werden alle Rollen aufgelistet. Dort eine auswählen
- auf der rechten Seite wird eine Vorschau der Attribute angezeigt
- `Add` fügt die Person mit der ausgewählten Rolle zum Interview hinzu. Die Person wird direkt
  geöffnet.

### Seite: Person

- oben rechts über den Papierkorb kann die Person gelöscht werden
- da drunter ist eine Liste von Feldern, die eingestellt werden können
- ganz unten ist die Dateiansicht
    - `Add folder to root` legt einen neuen Ordner in der Wurzelebene an. Der Name kann direkt
      bearbeitet werden
    - `Upload file to root` öffnet Upload file` modal
    - ein Rechtsklick auf einen Eintrag öffnet ein Menü. Gleiches geht auch mit den drei
      Punkten auf der rechten Seite.
        - `Upload` öffnet ein Uploadmodal. Wenn der aktuelle Eintrag eine Datei ist, dann wird als
          Ziel der Elternordner der Datei ausgewählt. Wenn der aktuelle Eintrag ein Ordner ist, dann
          dieser Ordner.
        - `Add Folder inside` legt einen neuen Order in diesem Eintrag an. Der Name kann direkt
          bearbeitet werden.
        - `Add Folder next to it` legt einen neuen Ordner direkt benachbart zum Eintrag an. Der Name
          kann direkt bearbeitet werden.
        - `Mark as confidential` verschiebt den Eintrag in die Confidential Database.
        - `Mark as non-confidential` verschiebt den Eintrag in die normale Database.
        - `Rename` benennt den aktuellen Eintrag um
        - `Delete` löscht den Eintrag
        - `Properties` öffnet das Vorschaumodal
    - ein Linksklick auf einen Eintrag öffnet das Vorschaumodal
    - Einträge lassen sich per Drag&Drop in Ordner verschieben

### Modal: Upload file

- `Upload` öffnet die Dateiauswahl des Browsers und Dateien werden automatisch ins Ziel hochgeladen.
- Eine Datei per Drag&Drop aus dem Dateiexplorer in dieses Modal führt auch ein Upload aus.

### Modal: File

- oben wird eine Liste von Eigenschaften zu dieser Datei angezeigt.
- Das Symbol links ist abhängig vom Dateityp. Ist der aktuelle Dateityp ein Bild, so wird dieses
  Bild angezeigt.
- Wenn die aktuelle Datei eine Audiodatei ist, so wird im mittleren Bereich ein Player angezeigt.
  Die Datei kann so lange abgespielt werden, solange das Modal geöffnet oder versteckt ist (die
  Audio wird weiter abgespielt, wenn das Modal in der Taskleiste ist).
- `Show History` öffnet die Historie im unteren Bereich
- `Preview` öffnet die Datei zur Vorschau im neuen Tab
- `Download` öffnet die Datei zum Download im neuen Tab
