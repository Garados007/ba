#!/bin/bash

trap "exit" INT TERM ERR
trap "kill 0" EXIT

elm-live \
    --port=8000 \
    --start-page=index.html \
    src/Main.elm \
    -x /api \
    -y http://localhost:8015 \
    -u \
    -- --output=index.js
