module Lazy exposing (..)

type Lazy loading loaded
    = Loading loading
    | Loaded loaded

toMaybe : Lazy a b -> Maybe b
toMaybe lazy =
    case lazy of
        Loading _ -> Nothing
        Loaded b -> Just b

mapLoading : (a1 -> a2) -> Lazy a1 b -> Lazy a2 b
mapLoading mapper =
    mapBoth mapper identity

mapLoaded : (b1 -> b2) -> Lazy a b1 -> Lazy a b2
mapLoaded =
    mapBoth identity

mapBoth : (a1 -> a2) -> (b1 -> b2) -> Lazy a1 b1 -> Lazy a2 b2
mapBoth mapperA mapperB lazy =
    case lazy of
        Loading a -> Loading <| mapperA a
        Loaded b -> Loaded <| mapperB b

unify : (a -> c) -> (b -> c) -> Lazy a b -> c
unify mapperLoading mapperLoaded lazy =
    case lazy of
        Loading a -> mapperLoading a
        Loaded b -> mapperLoaded b

isLoading : Lazy a b -> Bool
isLoading lazy =
    case lazy of
        Loading _ -> True
        Loaded _ -> False
