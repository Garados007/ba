module Tools.DeleteButton exposing (..)

import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Html.Events exposing (onClick)

view : msg -> Html msg
view onClick =
    div [ class "tools-delete-button" 
        , HE.onClick onClick
        ]
        [ Html.img
            [ HA.src "/img/svgrepo/essential-set-2/garbage-45573.svg"
            ] []
        , div [ class "tools-delete-button-text" ]
            [ text "Delete" ]
        ]
