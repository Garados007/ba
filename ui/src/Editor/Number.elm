module Editor.Number exposing (..)

import Html exposing (Html, div)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Triple exposing (Triple)
import Triple exposing (triple)

type alias Model =
    { text: String
    }

type Msg
    = Input String

init : Float -> Model
init value = { text = String.fromFloat value }

view : Bool -> Model -> Html Msg
view readonly { text } =
    div [ class "editor-single-line" ]
        [ Html.input
            [ HA.type_ "number"
            , HA.value text
            , HA.readonly readonly
            , HE.onInput Input
            ] []
        ]
    
update : Msg -> Model -> Triple Model (Cmd Msg) (Maybe Float)
update msg model =
    case msg of
        Input text ->
            triple
                { text = text }
                Cmd.none
            <| String.toFloat text
