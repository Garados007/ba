module Editor.MultiLine exposing (..)

import Html exposing (Html, div)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Triple exposing (Triple)
import Triple exposing (triple)

type alias Model =
    { text: String
    }

type Msg
    = Input String

init : String -> Model
init text = { text = text }

view : Bool -> Model -> Html Msg
view readonly { text } =
    div [ class "editor-multi-line" ]
        [ Html.textarea
            [ HA.value text
            , HA.readonly readonly
            , HE.onInput Input
            ] []
        ]
    
update : Msg -> Model -> Triple Model (Cmd Msg) (Maybe String)
update msg model =
    case msg of
        Input text ->
            triple
                { text = text }
                Cmd.none
            <| Just text
