module Panes.Special.MissingData exposing (..)

import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Panes exposing (Event)
import Network.Messages

viewMissingData : Html Event
viewMissingData =
    div [ class "pane-special-missing-data" ]
        [ Html.img
            [ HA.src "/img/svgrepo/essential-set-2/radar-126004.svg"
            ] []
        , Html.h2 []
            [ text "Requested data missing" ]
        , Html.p []
            [ text "Your requested data could not be shown. This could be because of one of the following reasons:" ]
        , Html.ol []
            [ Html.li []
                [ text "The containing database is not opened. You need to do open it to gain access again." ]
            , Html.li []
                [ text "You have permanetly deleted the data. There is no mean to restore this." ]
            ]
        , Html.p []
            [ text "If the containg database needs to be opened you can do this here:" ]
        , Html.button
            [ HE.onClick <| Panes.Send <| Network.Messages.DatabaseConnect Nothing ]
            [ text "Login to open Database" ]
        ]
