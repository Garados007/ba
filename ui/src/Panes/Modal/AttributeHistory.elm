module Panes.Modal.AttributeHistory exposing
    ( component
    , InitModel
    , Model
    , Msg
    )

import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Triple exposing (triple)
import Panes exposing (..)
import Data exposing (Id)
import Data.Schema
import Tools
import PaneMeta exposing (PaneMeta)
import History
import Url
import Iso8601
import Time exposing (Posix)
import Editor.Attributes

component : RawComponent InitModel Model Msg
component =
    { init = init
    , view = view
    , title = title
    , update = update
    , apply = apply
    , save = save
    , subscription = subscription
    }

type alias Model =
    { editor: Editor.Attributes.Editor
    , selected: Posix
    , person: Id
    , data: Data.Attribute
    , schema: Data.Schema.Attribute
    }

type alias InitModel = Panes.AttributeHistoryInit

type Msg
    = None
    | SendEvent Event
    | Select Posix
    | Wrap Editor.Attributes.Msg

init : Init InitModel Model Msg
init meta data =
    triple
        { editor = Editor.Attributes.initSingle
            meta.zoneName meta.zone meta.now
            data.person data.data data.schema
        , selected = data.data.entry.date
        , person = data.person
        , data = data.data
        , schema = data.schema
        }
        Cmd.none
        []

view : View Model Msg
view { meta, state } =
    div [ class "modal-attr-history" ]
        [ div [ class "modal-attr-history-entries" ]
            <| List.map
                (\entry ->
                    div [ HA.classList
                            [ ("modal-attr-history-entry", True)
                            , ("utils-link", True)
                            , Tuple.pair "modal-attr-history-entry-selected"
                                <| entry.date == state.selected
                            ]
                        , HE.onClick <| Select entry.date
                        ]
                        [ text <| Tools.viewDateTime
                            meta.zone
                            entry.date
                        ]
                )
            <| state.data.entries
        , div [ class "modal-attr-history-viewer" ]
            [ Html.map Wrap
                <| Editor.Attributes.viewSingle True meta.now state.editor
            ]
        ]

title : Title Model
title { state } =
    state.data.key ++ ": #" ++ state.person

update : Update Model Msg
update msg { meta, state } =
    case msg of
        None -> triple
            state
            Cmd.none
            []
        SendEvent event -> triple
            state
            Cmd.none
            [ event ]
        Select time -> triple
            { state
            | selected = time
            , editor =
                Maybe.withDefault state.editor
                <| Maybe.map
                    (\entry -> state.data |> \data ->
                        Editor.Attributes.initSingle
                            meta.zoneName meta.zone meta.now
                            state.person
                            { data
                            | entry = entry
                            }
                            state.schema
                    )
                <| List.head
                <| List.filter
                    (\x -> x.date == time)
                    state.data.entries
            }
            Cmd.none
            []
        Wrap sub ->
            Editor.Attributes.update
                meta.now
                sub
                state.editor
            |> Triple.mapAll
                (\x -> { state | editor = x })
                (Cmd.map Wrap)
                identity

apply : Apply Model Msg
apply event { state } =
    case event of
        _ -> triple state Cmd.none []

save : Save Model
save _ = []

subscription : Subscription Model Msg
subscription _ = Sub.none
