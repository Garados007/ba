module Panes.Search.View exposing (..)

import Data.Search exposing 
    ( SearchResponse, SearchResult, SearchResultType(..), SearchFound, SearchFoundType(..)
    , SearchValue(..)
    )
import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Html.Events as HE
import Html.Lazy as HL
import Panes exposing (Event)
import Network.Messages
import History
import Html exposing (Attribute)

type alias Send msg = Event -> msg

view : Send msg -> SearchResponse -> Html msg
view send response =
    div [ class "pane-search-view-response" ]
    <| (\list ->
        if response.isLast
        then list ++ [ viewLast ]
        else list
        )
    <| List.concatMap
        (\result ->
            (::) (HL.lazy (viewBox send) result)
            <| List.map
                (HL.lazy2 (viewFound send) result)
                result.found
        )
    <| response.result

viewBox : Send msg -> SearchResult -> Html msg
viewBox send result =
    div 
        [ class "pane-search-view-result"
        , resultEvent send result
        ]
        [ div [ class "pane-search-view-icon" ]
            [ Html.img
                [ HA.src
                    <| case result.type_ of
                        File -> "/img/svgrepo/essential-set-2/file-138907.svg"
                        Interview -> "/img/svgrepo/essential-set-2/notebook-36365.svg"
                        Person -> "/img/svgrepo/essential-set-2/user-13663.svg"
                ] []
            ]
        , div [ class "pane-search-view-details" ]
            [ div [ class "pane-search-view-title" ]
                [ case result.role of
                    Nothing -> text ""
                    Just role ->
                        div [ class "pane-search-view-role" ]
                            [ text role ]
                , div [ class "pane-search-view-title-name" ]
                    [ text result.title ]
                , div [ class "pane-search-view-title-database", class "utils-database" ]
                    [ text result.database ]
                ]
            , div [ class "pane-search-view-infos" ]
                <| List.map
                    (\{ name, value } ->
                        div [ class "pane-search-view-info" ]
                            [ div [ class "pane-search-view-info-name" ]
                                [ text name ]
                            , div [ class "pane-search-view-info-value" ]
                                [ text value ]
                            ]
                    )
                <| result.info
            ]
        ]

viewFound : Send msg -> SearchResult -> SearchFound -> Html msg
viewFound send result found =
    div
        [ class "pane-search-view-found"
        , resultEvent send result
        ]
        [ div
            [ HA.classList
                [ ("pane-search-view-found-field", True)
                , ("pane-search-view-found-is-attribute", found.type_ == Attribute)
                ]
            ]
            [ text found.field ]
        , div [ class "pane-search-view-found-value" ]
            <| List.map
                (\value ->
                    case value of
                        Ellipsis ->
                            div [ class "pane-search-view-found-ellipsis" ]
                                [ text "..." ]
                        Context val ->
                            div [ class "pane-search-view-found-context" ]
                                [ text val ]
                        Term val ->
                            div [ class "pane-search-view-found-term" ]
                                [ text val ]
                )
            <| found.value
        ]

viewLast : Html msg
viewLast =
    div [ class "pane-search-view-last" ]
        [ text "no more results" ]

resultEvent : Send msg -> SearchResult -> Html.Attribute msg
resultEvent send result =
    HE.onClick 
    <| send
    <| case result.type_ of
        Interview -> Panes.Open <| History.Interview result.id
        Person -> Panes.Open <| History.Person result.id
        File -> Panes.OpenFile result.id
