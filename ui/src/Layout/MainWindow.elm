module Layout.MainWindow exposing
    ( Layout
    , Progress
    , view
    )

import Html exposing (Html, div, text)
import Html.Attributes as HA exposing (class)
import Time exposing (Posix, Zone)
import Tools
import Notification

type alias Layout msg =
    { title: String
    , sideNav: Html msg
    , pane: Html msg
    , progress: List Progress
    , zone: Zone
    , now: Posix
    , modal: Html msg
    , taskbar: Html msg
    , notification: Notification.Model
    , wrapNotification: Notification.Msg -> msg
    }

type alias Progress =
    { title: String
    , fill: Maybe Float
    }

view : Layout msg -> Html msg
view layout =
    div [ class "layout-main-window" ]
        [ div [ class "layout-main-window-title-bar" ]
            [ Html.img
                [ class "layout-main-window-logo"
                , HA.src "/img/Minerva_RGB/SVG/MPG_Minerva_RGB_mpg-green.svg"
                ] []
            , div [ class "layout-main-window-title" ]
                [ text layout.title ]
            ]
        , div [ class "layout-main-window-body" ]
            [ div [ class "layout-main-window-nav" ]
                [ layout.sideNav ]
            , div []
                [ div [ class "layout-main-window-content" ]
                        [ layout.pane ]
                , div [ class "layout-main-window-progress-list" ]
                    <| List.map viewProgress layout.progress
                ]
            ]
        , layout.taskbar
        , layout.modal
        , Notification.view layout.now layout.notification
            |> Html.map layout.wrapNotification
        ]

viewProgress : Progress -> Html msg
viewProgress progress =
    div [ class "layout-main-window-progress" ]
        [ div [ class "layout-main-window-progress-title" ]
            [ text progress.title ]
        , div [ class "layout-main-window-progress-bar" ]
            [ case progress.fill of
                Just fill ->
                    div
                        [ class "layout-main-window-progress-fill" 
                        , HA.style "width"
                            <| (String.fromFloat <| clamp 0 100 <| 100 * fill)
                            ++ "%"
                        ] []
                Nothing ->
                    div
                        [ class "layout-main-window-progress-fill"
                        , class "marque"
                        ] []
            ]
        ]