port module Ports exposing (..)

import Json.Decode as JD
import Json.Encode as JE

port receiveSocketMsg : (JD.Value -> msg) -> Sub msg
port sendSocketCommand : JE.Value -> Cmd msg
port receiveSocketClose : (JD.Value -> msg) -> Sub msg

port fido2receive : (JD.Value -> msg) -> Sub msg
port fido2send : JE.Value -> Cmd msg
