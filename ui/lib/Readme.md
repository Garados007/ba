# External Elm Libraries

Das sind Elm Bibliotheken, bei denen ich Funktionen reingepatched habe, welche ich benötige, aber
diese nicht nativ haben. Es existieren schon Pull-Requests dazu, aber bis das durch ist, kann da
schon einiges an Zeit vergehen - von daher kopiere ich einfach den Code hier rein.

Sobald ich sehe, dass der Merge durch ist und die Funktionen nun auch für mich nutzbar sind,
entferne ich den Code hier wieder und binde die Pakete wieder ein.

## [jinjor/elm-contextmenu](https://package.elm-lang.org/packages/jinjor/elm-contextmenu/latest/)

- Aktuelle Version (bekannt): 2.0.0
- Pull Request: [add the ability for a left click *(\#6)*](https://github.com/jinjor/elm-contextmenu/pull/6)
- Code: [elm-contextmenu](elm-contextmenu)

